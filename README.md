# LedWall Android Application #

* *A simple android app that shows what can be done with the LedWall*
* Version 4.1
* June 8. 2016
* Tested and working on Android 5.1.1 Lollipop on Sony Xperia Z3C (API21)

## Features ##

* Sending messages to the LedWall
* Playing snake on the LedWall using the phone as an input device
* Playing connect4 on the LedWall using the phone as an input device
* Playing minesweeper on the LedWall using the phone as an input device
* Displaying highest scores in snake which are stored on a remote server (http://vm23.htl-leonding.ac.at:8080/score-server/)

## Requirements ##

* Smartphone running on Android API 19 or higher
* LedWall + Arduino with LedwallArduinoProgram v3b1 (https://bitbucket.org/ledwall/lap-repo)
* Score-Server v4 (https://bitbucket.org/ledwall/ledwallscoreserver)

## Setup ##

* Install the .apk file on your mobile device
* Set up your Arduino and LedWall (click link above to get instructions)

## Creators ##

+ *Ralph Mosquera* 
+ *Sebastian Tanzer*
+ *Christopher Stelzmüller* ([mail](mailto:c.stelzmueller@yahoo.com))

*Project migrated from Subversion (old version can still be found at https://subversion.htl-leonding.ac.at/trac/2015LedWall1516)*

*Other parts of Project LedWall can be found here https://bitbucket.org/ledwall/*