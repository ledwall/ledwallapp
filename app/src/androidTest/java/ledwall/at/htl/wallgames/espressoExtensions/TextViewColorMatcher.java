package ledwall.at.htl.wallgames.EspressoExtensions;

import android.view.View;
import android.widget.TextView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

/**
 * Created by stoez on 28.05.2016.
 */
public class TextViewColorMatcher {
    public static Matcher<View> withColor(final int expectedColor) {
        return new TypeSafeMatcher<View>() {

            @Override
            public boolean matchesSafely(View view) {
                if (!(view instanceof TextView)) {
                    return false;
                }

                int textColor = ((TextView)view).getCurrentTextColor();

                return textColor == expectedColor;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has color " + expectedColor);
            }
        };
    }
}
