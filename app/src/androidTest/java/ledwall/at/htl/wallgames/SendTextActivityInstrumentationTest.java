package ledwall.at.htl.wallgames;

import android.graphics.Color;
import android.support.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static ledwall.at.htl.wallgames.EspressoExtensions.TextViewColorMatcher.withColor;
import static org.hamcrest.CoreMatchers.allOf;

/**
 * Instrumentation tests for SendTextActivity
 * @author stoez
 * @version 3.0b 5/27/2016
 */
public class SendTextActivityInstrumentationTest {
    @Rule
    public ActivityTestRule<SendTextActivity> activityTestRule =
            new ActivityTestRule<>(SendTextActivity.class);

    /**
     * Check if a regular message can be sent. This message doesn't contain any bad language, so no
     * part of the message should be filtered. The message color is not tested but should default to red (255,0,0)
     * @throws InterruptedException
     *      is thrown when the Thread is interrupted while sleeping, shouldn't occur...
     */
    @Test
    public void t01_sendRegularText() throws InterruptedException {
        String messageText = "Good Morning Espresso";
        int defaultColor = Color.RED;

        doBasicInput(messageText);

        onView(allOf(withId(android.support.design.R.id.snackbar_text), withText("Message sent")))
                .check(matches(isDisplayed()));
        onView(withId(R.id.slidingText)).check(matches(allOf(withText(messageText),
                withColor(defaultColor))));
    }

    /**
     * Checks if a test containing swear words (in this case "bitch") is filtered out.
     * Upon trying to send a message with bad language, a different message should be displayed in the snackbar
     * @throws InterruptedException
     *      is thrown when the Thread is interrupted while sleeping, shouldn't occur...
     */
    @Test
    public void t02_sendTextWithSwearWords() throws InterruptedException{
        String messageText = "Jesus is a bitch";

        doBasicInput(messageText);

        onView(withId(android.support.design.R.id.snackbar_text))
                .check(matches(withText(R.string.message_not_sent_text)));
        onView(withId(R.id.slidingText)).check(matches(withText("")));
    }


    /**
     * Checks if the values set in the ColorPicker are analyzed the right way. Unfortunately,
     * clicking in the ColorPicker, or sliding the ValueBar are not easily realizable with espresso.
     * @throws InterruptedException
     *      is thrown when the Thread is interrupted while sleeping, shouldn't occur...
     */
    @Test
    public void t11_setTextColor() throws InterruptedException{
        int expectedColor = Color.WHITE;
        String messageText = "Hi Espresso!";
        final SendTextActivity activity = activityTestRule.getActivity();

        onView(withId(R.id.btn_changeColour)).perform(click());
        activity.cdd.colorPicker.post(new Runnable() {
            @Override
            public void run() {
                activity.cdd.colorPicker.setColor(Color.WHITE);
            }
        });
        onView(withId(R.id.selectBtn)).perform(click());

        doBasicInput(messageText);

        onView(allOf(withId(android.support.design.R.id.snackbar_text), withText("Message sent")))
                .check(matches(isDisplayed()));
        onView(withId(R.id.slidingText)).check(matches(allOf(withText(messageText),
                withColor(expectedColor))));
    }

    /**
     * Does the basic input for the MessageSender activity
     * @param input
     *      the string that should be written into the message-TextView
     * @throws InterruptedException
     *      is thrown when the Thread is interrupted while sleeping, shouldn't occur...
     */
    private void doBasicInput(String input) throws InterruptedException {
        onView(withId(R.id.message_text)).perform(typeText(input))
                .check(matches(withText(input)))
                .perform(closeSoftKeyboard());
        Thread.sleep(20);
        onView(withId(R.id.fabSend)).perform(click());
    }
}
