package ledwall.at.htl.wallgames;

import android.support.test.rule.ActivityTestRule;
import android.widget.TableLayout;
import android.widget.TableRow;

import org.junit.Rule;
import org.junit.Test;

import ledwall.at.htl.wallgames.Entity.ConnectFourBlock;
import ledwall.at.htl.wallgames.Logic.ConnectFourGame;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Instrumentation Tests for Connect 4 Game
 * @author stoez
 * @version 1.0 08.06.2016
 */
public class ConnectFourInstrumentationTest {
    @Rule
    public ActivityTestRule<ConnectFourActivity> activityActivityTestRule =
            new ActivityTestRule<>(ConnectFourActivity.class);

    /**
     * Checks if the game board is set up correctly
     */
    @Test
    public void t01_gameSetup(){
        onView(withId(R.id.btn_restart)).check(matches(isDisplayed()));
        onView(withId(R.id.btn_restart)).perform(click());

        ConnectFourGame game = activityActivityTestRule.getActivity().getGame();

        TableLayout tl = (TableLayout) activityActivityTestRule
                .getActivity().findViewById(R.id.tl_connectFourField);
        for(int i = 0; i < tl.getChildCount(); i++){
            if(tl.getChildAt(i) instanceof TableRow) {
                TableRow r = (TableRow) tl.getChildAt(i);
                for (int c = 0; c < r.getChildCount(); c++)     {
                    System.out.println(i);
                    assertThat(((ConnectFourBlock) r.getChildAt(c)).getState(), is(0));
                }
            }
        }
    }

    @Test
    public void t02_easyGameWin(){
        onView(withId(R.id.btn_restart)).perform(click());

        final ConnectFourGame game = activityActivityTestRule.getActivity().getGame();
        activityActivityTestRule.getActivity().findViewById(R.id.tl_connectFourField).post(new Runnable() {
            @Override
            public void run() {
                game.dropTokenInColumn(1);
                game.dropTokenInColumn(2);
                game.dropTokenInColumn(1);
                game.dropTokenInColumn(2);
                game.dropTokenInColumn(1);
                game.dropTokenInColumn(2);
                game.dropTokenInColumn(1);
            }
        });

        String winText = activityActivityTestRule.getActivity().getString(R.string.game_won_portrait_format);
        winText = String.format(winText, game.getCurrentPlayer());
        onView(withText(winText)).check(matches(isDisplayed()));
    }
}
