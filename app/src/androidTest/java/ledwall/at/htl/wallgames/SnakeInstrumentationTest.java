package ledwall.at.htl.wallgames;

import android.content.SharedPreferences;
import android.support.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import java.util.Locale;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
/**
 * Instrumentation tests for SendTextActivity
 * @author stoez
 * @version 3.0b 5/30/2016
 */
public class SnakeInstrumentationTest {
    @Rule
    public ActivityTestRule<SnakeActivity> activityTestRule =
            new ActivityTestRule<>(SnakeActivity.class);

    /**
     * Checks if starting a new game and leaving the game after dying is working correctly.
     * Takes approximately 11 seconds to complete because espresso has to wait until the game has been finished
     * @throws InterruptedException
     *      is thrown when the Thread is interrupted while sleeping, shouldn't occur...
     */
    @Test
    public void t01_startGameAndExitAfterDeath() throws InterruptedException {
        onView(withText("yep!")).perform(click());
        Thread.sleep(7000);

        onView(withText(R.string.game_over_title)).check(matches(isDisplayed()));
        onView(withId(R.id.btnExit)).perform(click());

        onView(withText(R.string.credits_names)).check(matches(isDisplayed()));
        onView(withText(R.string.home_text_portrait)).check(matches(isDisplayed()));
    }

    /**
     * Checks if current score and highscore are displayed in the right way.
     * Fetches the current highscore from SharedPreferences. Values for highscore is not the same
     * on all devices, obviously...
     */
    @Test
    public void t02_highScoreAndScoreDisplayedCorrectly(){
        SnakeActivity activity = activityTestRule.getActivity();
        SharedPreferences prefs = activity.getLedWallPrefs();
        int highscore = prefs.getInt(activity.getApplicationContext().getString(
                R.string.snake_highscore_preference_key), 0);
        onView(withText("yep!")).perform(click());
        onView(withId(R.id.tv_score)).check(matches(
                withText(String.format(Locale.GERMAN,
                        activity.getApplicationContext().getString(R.string.score_format_string), 0))));
        onView(withId(R.id.tv_highScore)).check(matches(withText(String.format(Locale.GERMAN,
                activity.getApplicationContext().getString(R.string.highscore_format_string), highscore))));
    }
}
