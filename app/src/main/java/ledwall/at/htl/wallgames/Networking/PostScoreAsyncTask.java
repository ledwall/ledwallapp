package ledwall.at.htl.wallgames.Networking;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import ledwall.at.htl.wallgames.Entity.SnakeScore;
import ledwall.at.htl.wallgames.R;

/**
 * AsyncTask that is used to send a new SnakeScore to a server
 * The server's URI is stored in a string resource
 * @author stoez
 * @version 1.0 23/5/2016
 */
public class PostScoreAsyncTask extends AsyncTask<Void, Void, Boolean> {
    private final String TAG = getClass().getSimpleName();
    private Context mContext;
    private SnakeScore mScore;

    public PostScoreAsyncTask(Context mContext, SnakeScore mScore) {
        super();
        this.mContext = mContext;
        this.mScore = mScore;
    }

    /**
     * The code the task will try to execute in the background. An AsyncTask is used to send data without
     * blocking the UI-thread.
     * @param params
     *      will be ignored because the task can only post one score that is given upon initialization
     *      in the constructor.
     * @return
     *      a boolean indicating if the score has been sent successfully
     */
    @Override
    protected Boolean doInBackground(Void... params) {

        URL url;
        HttpURLConnection client = null;
        try {
            url = new URL(String.format(mContext.getString(R.string.score_url_parse_string), mContext.getString(R.string.score_server_ip)));
            client = (HttpURLConnection) url.openConnection();

            client.setRequestMethod("POST");
            client.setRequestProperty("Content-Type", "application/json");
            client.setConnectTimeout(1000);
            client.setDoOutput(true);

            BufferedWriter outputWriter = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
            outputWriter.append(mScore.toJsonString());
            outputWriter.flush();
            outputWriter.close();

            int resCode = client.getResponseCode();

            client.disconnect();
            return resCode == 201;

        } catch (IOException e) {
            Log.e(TAG, "Can't connect to score-server on " + mContext.getString(R.string.score_server_ip), e);
        } finally {
            if (client != null) {
                client.disconnect();
            }
        }

        return false;
    }

    /**
     * This code will be executed after the score has been sent to the server and is doing one of two things
     * depending on the success of the request to the server.
     * @param expectedResponseCode
     *      boolean indicating if the server accepted the sent score or not
     *      if the score has been retrieved, it will be removed from SharedPreferences,
     *      otherwise it will be stored in SharedPreferences again.
     */
    @Override
    protected void onPostExecute(Boolean expectedResponseCode) {
        Log.d(TAG, expectedResponseCode ? "The score was created" : "The score couldn't be created");
        if (!expectedResponseCode) {
            SharedPreferences highscorePrefs = mContext.getSharedPreferences(mContext.getString(R.string.highscores_shared_preference_key), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = highscorePrefs.edit();
            editor.putString(mContext.getString(R.string.highscore_object_preference_key), mScore.toJsonString());
            editor.apply();
            Log.d(TAG, "onPostExecute: The score was stored in SharedPreferences and will be sent later...");
        } else {
            SharedPreferences highscorePrefs = mContext.getSharedPreferences(mContext.getString(R.string.highscores_shared_preference_key), Context.MODE_PRIVATE);
            if (highscorePrefs.contains(mContext.getString(R.string.highscore_object_preference_key))) {
                SharedPreferences.Editor editor = highscorePrefs.edit();
                editor.remove(mContext.getString(R.string.highscore_object_preference_key));
                editor.apply();
            }
        }
    }
}