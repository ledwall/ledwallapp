package ledwall.at.htl.wallgames.Entity;

import android.content.Context;
import android.util.Log;
import android.widget.Button;
import ledwall.at.htl.wallgames.R;
import org.bitbucket.tuesd4y.ledwallapi.entity.Block;
import org.json.JSONObject;

/**
 * Block which is used by the Connect4Game, unfortunately cannot extend two classes at the same time,
 * so the toJsonObject has to be implemented yet again.
 * @author stoez
 * @version 2.0 3/29/2016
 */
public class ConnectFourBlock extends Button implements Block {
    private final String TAG = getClass().getSimpleName();

    private int state;
    private int row;
    private int col;

    public ConnectFourBlock(Context context, int row, int col) {
        super(context);
        this.row = row;
        this.col = col;
    }

    public int getState() {
        return state;
    }

    /**
     * changes the blocks state and its background color/drawable resource
     * @param state
     *      the new state of the block
     */
    public void setState(int state) {
        this.state = state;
        if(state == 0){
            setBackgroundResource(R.drawable.square_pink);
        } else if(state == 1){
            setBackgroundResource(R.drawable.square_blue);
        } else if(state == 2){
            setBackgroundResource(R.drawable.square_red);
        }
    }

    public int getRow() {
        return row+1;
    }

    @Override
    public int getColumn() {
        return col+1;
    }

    @Override
    public int getType() {
        return state;
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject b = new JSONObject();
        try{
            b.put("row", getRow());
            b.put("col", getColumn());
            b.put("colour", getType()+1);
        } catch (Exception ex){
            //throw new JSONException("failed to add data to json object");
            //no idea when this happens so...
            Log.e(TAG, "toJsonObject: BaseBlock cannot be converted to JsonObject", ex);
        }
        return b;
    }
}
