package ledwall.at.htl.wallgames.Entity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.sql.Date;

import ledwall.at.htl.wallgames.Logic.SnakeGame;

/**
 * Database Representation of a Score reached while playing snake. Scores can be posted to and
 * downloaded from a score-server in the DisplaySnakeScoresActivity
 * @author stoez
 * @version 2.0 5/19/2016
 */
public class SnakeScore implements Serializable{
    private int score;
    private String name;
    private Date created;

    public SnakeScore(JSONObject jsonObject){
        try {
            this.score = jsonObject.getInt("score");
            this.name = jsonObject.getString("name");

            String dateParts[] = jsonObject.getString("created").split("-");
            int year = Integer.parseInt(dateParts[0])-1900;
            int month = Integer.parseInt(dateParts[1]);
            int day = Integer.parseInt(dateParts[2]);

            //noinspection deprecation
            this.created = new Date(year, month, day);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SnakeScore(int score, String name, Date created) {
        this.score = score;
        this.name = name;
        this.created = created;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String toJsonString(){
        return "{\"score\":" + score + ",\"name\":\"" + name + "\",\"created\":\"" + created.toString() + "\"}";
    }
}
