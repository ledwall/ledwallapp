package ledwall.at.htl.wallgames.Business

import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.View

import ledwall.at.htl.wallgames.R

@Suppress("DEPRECATION")
/**
 * @author stoez
 * @version 1.0 31.05.2016
 */

class SimpleDividerItemDecoration(resources: Resources) : RecyclerView.ItemDecoration() {
    private val mDivider: Drawable

    init {
        mDivider = resources.getDrawable(R.drawable.line_divider)
    }

    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State?) {
        val left = parent.paddingLeft
        val right = parent.width - parent.paddingRight

        val childCount = parent.childCount
        for (i in 0..childCount - 1) {
            val child = parent.getChildAt(i)

            val params = child.layoutParams as RecyclerView.LayoutParams

            val top = child.bottom + params.bottomMargin
            val bottom = top + mDivider.intrinsicHeight

            mDivider.setBounds(left, top, right, bottom)
            mDivider.draw(c)
        }
    }
}
