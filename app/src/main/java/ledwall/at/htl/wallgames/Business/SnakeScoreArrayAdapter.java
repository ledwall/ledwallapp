package ledwall.at.htl.wallgames.Business;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import ledwall.at.htl.wallgames.Entity.SnakeScore;
import ledwall.at.htl.wallgames.R;

/**
 * An ArrayAdapter which helps access the list of SnakeScores fetched from the score-server
 * @author stoez
 * @version 1.0 20.05.2016.
 *
 * @deprecated
 */
public class SnakeScoreArrayAdapter extends ArrayAdapter<SnakeScore> {
    public SnakeScoreArrayAdapter(Context context, List<SnakeScore> scores) {
        super(context, -1, scores);
    }

    private class ViewHolder{
        TextView nameText;
        TextView scoreText;
        TextView dateCreatedText;
    }

    @SuppressWarnings("deprecation")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        SnakeScore s = getItem(position);
        ViewHolder viewHolder;

        if(rowView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            rowView = inflater.inflate(R.layout.list_item, parent, false);
            viewHolder.nameText = (TextView) rowView.findViewById(R.id.name);
            viewHolder.scoreText = (TextView) rowView.findViewById(R.id.score);
            viewHolder.dateCreatedText = (TextView) rowView.findViewById(R.id.created);

            rowView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) rowView.getTag();
        }

        viewHolder.nameText.setText(s.getName());
        String dateString = String.format(Locale.GERMAN, "%d. %d. %d", s.getCreated().getDay(),
                s.getCreated().getMonth(), s.getCreated().getYear()+1900);
        viewHolder.dateCreatedText.setText(dateString);
        viewHolder.scoreText.setText(Integer.toString(s.getScore()));

        return rowView;
    }
}
