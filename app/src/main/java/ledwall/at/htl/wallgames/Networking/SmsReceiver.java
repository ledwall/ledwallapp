package ledwall.at.htl.wallgames.Networking;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;
import ledwall.at.htl.wallgames.R;
import org.bitbucket.tuesd4y.ledwallapi.entity.LedWallMode;
import org.bitbucket.tuesd4y.ledwallapi.networking.MessageSender;

import java.net.InetAddress;

/**
 * This class provides a method that will be executed every time a certain event, in this case the
 * receive of a new sms-message happens.
 * @author stoez
 * @version 1.0 3/30/2016
 */
public class SmsReceiver extends BroadcastReceiver {
    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private final String TAG = getClass().getSimpleName();

    /**
     * this method will be executed every time a new sms has been received
     * @param context
     *      application context, useful for opening SharedPreferences
     * @param intent
     *      the intent which opened this receiver
     */
    @Override
    public void onReceive(Context context, Intent intent) {

        SharedPreferences ledWallPrefs = context.getSharedPreferences(
                context.getString(R.string.shared_preferences_key), Context.MODE_PRIVATE);

        String ip = ledWallPrefs.getString(
                context.getString(R.string.ip_preference_key),
                context.getString(R.string.default_ip));
        int port = (ledWallPrefs.getInt(
                context.getString(R.string.port_preference_key),
                context.getResources().getInteger(R.integer.default_port)));
        MessageSender sender;
        try {
            sender = new UdpSender(InetAddress.getByName(ip), port);
        } catch (Exception ex) {
            sender = null;
            Log.e(TAG, "Couldn't create Udp-Sender", ex);
        }

        Boolean listenForSms = ledWallPrefs.getBoolean(context.getString(R.string.listen_for_sms_key),
                context.getResources().getBoolean(R.bool.default_listen_for_sms_value));

        if (intent.getAction().equals(SMS_RECEIVED) && listenForSms) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                // get sms objects
                Object[] pdus = (Object[]) bundle.get("pdus");
                if (pdus != null && pdus.length == 0) {
                    return;
                }
                // large message might be broken into many
                SmsMessage[] messages = new SmsMessage[pdus.length];
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < pdus.length; i++) {
                    //noinspection deprecation
                    messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    sb.append(messages[i].getMessageBody());
                }
                String messageOriginAddress = messages[0].getOriginatingAddress();
                String message = sb.toString();
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                // prevent any other broadcast receivers from receiving broadcast
                abortBroadcast();
                Log.i(TAG, "Received Sms from " + messageOriginAddress + " with content: " + message);

                if(sender != null){
                    sender.sendModeChange(LedWallMode.MESSAGE);
                    sender.sendMessage(message);
                }
            }
        }
    }
}