package ledwall.at.htl.wallgames;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

/**
 * @author Tanzer, stoez
 * @version 5/23/2016
 */
public class MenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    protected DrawerLayout mDrawer;
    private LinearLayout mTopLayout;
    private LinearLayout mBottomLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        //region fetching view references
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        setSupportActionBar(toolbar);
        mBottomLayout = (LinearLayout) findViewById(R.id.menuBackgroundBottom);
        mTopLayout = (LinearLayout) findViewById(R.id.menuBackgroundTop);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        //endregion

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent i = new Intent(MenuActivity.this, SettingsActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent i = null;
        switch (id){
            case R.id.nav_home:
                i = new Intent(this, HomeActivity.class);
                break;
            case R.id.nav_fourInARow:
                i = new Intent(this, ConnectFourActivity.class);
                break;
            case R.id.nav_minesweeper:
                i = new Intent(this, MinesweeperActivity.class);
                break;
            case R.id.nav_show_scores:
                i = new Intent(this, DisplaySnakeScoresActivity.class);
                break;
            case R.id.nav_send:
                i = new Intent(this, SendTextActivity.class);
                break;
            case R.id.nav_snake:
                i = new Intent(this, SnakeActivity.class);
                break;
        }
        if(i != null){
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Sets the background color of the upper and lower linear layout on the screen
     * otherwise, two different colors would be used...
     * @param color
     *      The Color to use as a background color
     */
    public void useJustOneColor(int color){
        mTopLayout.setBackgroundColor(color);
        mBottomLayout.setBackgroundColor(color);
    }

    /**
     * sets the background color of the upper and lower linear layout to the default color, which
     * is fetched from a color resource file.
     * A deprecated method has to be used to support android versions older that API23
     */
    @SuppressWarnings("deprecation")
    public void useJustOneColor(){
        useJustOneColor(getResources().getColor(R.color.lightblue));
    }
}
