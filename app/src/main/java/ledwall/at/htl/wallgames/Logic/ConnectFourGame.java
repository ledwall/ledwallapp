/**
 * Created by Mosquera on 21.01.2016.
 * Modified by stoez on 3/24/2016
 */

package ledwall.at.htl.wallgames.Logic;

import android.content.Context;
import android.util.Log;
import ledwall.at.htl.wallgames.Business.OnConnectFourWinCallback;
import ledwall.at.htl.wallgames.Entity.ConnectFourBlock;
import org.bitbucket.tuesd4y.ledwallapi.entity.LedWallMode;
import org.bitbucket.tuesd4y.ledwallapi.networking.MessageSender;

import java.util.Random;

/**
 * @author Mosquera, stoez
 * @version 4/1/2016
 */
public class ConnectFourGame {
    private final String TAG = getClass().getSimpleName();

    public ConnectFourBlock table[][];
    public static final int ROWS = 7;
    public static final int COLUMNS = 14;
    private int currentPlayer;
    private boolean isOver;

    private Context context;
    private OnConnectFourWinCallback onWinCallback = null;
    private MessageSender sender;

    public ConnectFourGame(Context context) {
        Random rnd = new Random();
        this.currentPlayer = rnd.nextInt(2)+1;
        this.context = context;
        initializeField();
    }

    public void setOnWinCallback(OnConnectFourWinCallback onWinCallback) {
        this.onWinCallback = onWinCallback;
    }

    public ConnectFourGame(int playerStarting, Context context){
        if(playerStarting < 1 || playerStarting > 2)
            throw new IllegalArgumentException();

        this.context = context;
        this.currentPlayer = playerStarting;
        initializeField();
    }

    /**
     * Tries to drop a token in the given column
     * @param column    the column to drop the token in
     * @return  a boolean indicating if the drop was possible
     */
    public boolean dropTokenInColumn(int column){
        if(column < 0 || column >= COLUMNS)
            return false;

        for (int row = ROWS-1; row >= 0; row--) {
            if(setPlayerToken(row, column)){
                if(sender != null)
                    sender.sendBlock(table[row][column]);

                if(checkForWinByCurrentPlayer()){
                    //current player won...
                    isOver = true;
                    if(onWinCallback != null)
                        onWinCallback.callOnWin();
                }
                else{
                    changeCurrentPlayer();
                }
                return true;
            }
        }
        return false;
    }

    /**
     * resets the game to default values
     */
    public void resetGame(){
        if(sender != null){
            sender.sendModeChange(LedWallMode.GAME);
        }
        for (int r = 0; r<ROWS;r++) {
            for (int c = 0; c < COLUMNS; c++) {
                table[r][c].setState(0);
            }
        }
        isOver = false;
    }

    public boolean isOver() {
        return isOver;
    }

    /**
     * Ask who is playing
     * @return the current player
     */
    public int getCurrentPlayer() {
        return currentPlayer;
    }

    public void changeCurrentPlayer(){
        currentPlayer = currentPlayer == 1 ? 2 : 1;
    }

    /**
     *Always Before playing! Initialize the field and set the default value 0
     */
    public void initializeField()
    {
        table = new ConnectFourBlock[ROWS][COLUMNS];
        for (int r = 0; r<ROWS;r++) {
            for (int c = 0; c < COLUMNS; c++) {
                table[r][c] = new ConnectFourBlock(context, r, c);
            }
        }
    }

    /**
     * set 1 or 2 to the block with the same row and col
     * @param row
     * @param col
     * @return true => block is occupied with the correct player token ( 1 or 2 )
     *         false => block is already occupied, retry!
     */
    @SuppressWarnings("JavaDoc")
    public boolean setPlayerToken(int row, int col) {
        if (table[row][col].getState() == 0) {
            table[row][col].setState(getCurrentPlayer());
            return true;
        }
        return false;
    }

    /**
     * Check if the current player has won the game
     * @return true if he won, otherwise false
     */
    public boolean checkForWinByCurrentPlayer() {
        for(int c = 0; c < COLUMNS; c++) {
            for (int r = 0; r < ROWS - 3; r++) {
                boolean fourInARow = true;
                for (int i = 0; i < 4; i++) {
                    fourInARow &= table[r+i][c].getState() == currentPlayer;
                }
                if(fourInARow) {
                    Log.d(TAG, "won vertically in column " + c);
                    return true;
                }
            }
        }

        for(int r = 0; r < ROWS; r++) {
            for (int c = 0; c < COLUMNS - 3; c++) {
                boolean fourInARow = true;
                for (int i = 0; i < 4; i++) {
                    fourInARow &= table[r][c+i].getState() == currentPlayer;
                }
                if(fourInARow) {
                    Log.d(TAG, "won horizontally in row " + r);
                    return true;
                }
            }
        }

        for(int r = 0; r < ROWS-3; r++) {
            for (int c = 0; c < COLUMNS-4; c++) {
                boolean fourInARow1 = true;
                boolean fourInARow2 = true;
                for (int i = 0; i < 4; i++) {
                    fourInARow1 &= table[r+i][c+i].getState() == currentPlayer;
                    fourInARow2 &= table[ROWS-1-r-i][c+i].getState() == currentPlayer;
                }
                if(fourInARow1 || fourInARow2){
                    Log.d(TAG, "won diagonally in col " + c + " and row " + r);
                    return true;
                }
            }
        }

        return false;

    }

    public void setSender(MessageSender sender) {
        this.sender = sender;
    }
}
