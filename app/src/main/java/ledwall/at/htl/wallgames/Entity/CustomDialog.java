package ledwall.at.htl.wallgames.Entity;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Window;
import android.widget.Button;

import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.SaturationBar;

import ledwall.at.htl.wallgames.R;

/**
 * Dialog which shows a ColorPicker with a circular color selection tool and a SaturationBar.
 * @author Tanzer
 * @version 1.0 5/23/2016
 */
public class CustomDialog extends Dialog {

    public ColorPicker colorPicker;
    private int mColor;
    private Activity mActivity;
    private Dialog mDialog;
    private SaturationBar mSaturationBar;
    private Button mSelectButton;


    public CustomDialog(Activity a) {
        super(a);
        this.mActivity = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog);

        colorPicker = (ColorPicker) findViewById(R.id.picker);
        mSaturationBar = (SaturationBar) findViewById(R.id.valuebar);
        mSelectButton = (Button) findViewById(R.id.selectBtn);

        colorPicker.setColor(Color.RED);
        colorPicker.addSaturationBar(mSaturationBar);

    }


    public int getColorValue() {
        return mColor;
    }
}
