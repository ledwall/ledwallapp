package ledwall.at.htl.wallgames;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.*;
import android.widget.*;
import ledwall.at.htl.wallgames.Business.ElementSetter;
import ledwall.at.htl.wallgames.Entity.MinesweeperBlock;
import ledwall.at.htl.wallgames.Entity.SimpleBlock;
import ledwall.at.htl.wallgames.Logic.MinesweeperGame;
import ledwall.at.htl.wallgames.Networking.UdpSender;
import org.bitbucket.tuesd4y.ledwallapi.entity.Block;
import org.bitbucket.tuesd4y.ledwallapi.entity.LedWallMode;
import org.bitbucket.tuesd4y.ledwallapi.networking.MessageSender;

import java.net.InetAddress;
import java.util.List;
import java.util.Random;

/**
 * @author Mosquera, minor changes by Tanzer & stoez
 * @version 5/23/2016
 */
public class MinesweeperActivity extends MenuActivity {
    private final String TAG = getClass().getSimpleName();

    public TextView txtMineCount;
    public TextView txtTimer;
    public ImageButton btnSmile;
    public TableLayout mineField; // table layout to add mines to
    public LinearLayout botLayout;

    public MessageSender sender;
    public MinesweeperBlock blocks[][]; // blocks for mine field

    public int blockDimension = 19; // width of each block
    public int blockPadding = 2; // padding between blocks
    // Don't touch the default values!
    // LED-WALL 7x14
    public int numberOfRowsInMineField = 7; //LED-WALL ROWS
    public int numberOfColumnsInMineField = 14; // LED-WALL COLUMNS
    public int totalNumberOfMines = 20; //DEFAULT VALUE
    //SnakeBlock size
    public double height = 50;
    public int width = 2;
    // timer to keep track of time elapsed
    public Handler timer = new Handler();
    public int secondsPassed = 0;
    public boolean isTimerStarted; // check if timer already started or not
    public boolean areMinesSet; // check if mines are planted in blocks
    public boolean isGameOver; // check if game is over
    public int minesToFind; // number of mines yet to be discovered
    public MinesweeperGame game;
    private double standardMax = 1082;
    private double standardMinPortrait = 520;
    private double getStandardMinLandscape = 600;
    private int difference = 30;
    // timer call back when timer is ticked
    private Runnable updateTimeElasped = new Runnable() {
        public void run() {
            long currentMilliseconds = System.currentTimeMillis();
            ++secondsPassed;

            if (secondsPassed < 10) {
                txtTimer.setText("00" + Integer.toString(secondsPassed));
            } else if (secondsPassed < 100) {
                txtTimer.setText("0" + Integer.toString(secondsPassed));
            } else {
                txtTimer.setText(Integer.toString(secondsPassed));
            }

            // add notification
            timer.postAtTime(this, currentMilliseconds);
            // notify to call back after 1 seconds
            // basically to remain in the timer loop
            timer.postDelayed(updateTimeElasped, 1000);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Display display = ((WindowManager)getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
        final int currentOrientation = display.getRotation();


        if(currentOrientation == Surface.ROTATION_0){
            LayoutInflater inflater = (LayoutInflater) this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View contentView = inflater.inflate(R.layout.activity_minesweeper, null, false);
            mDrawer.addView(contentView, 1);

            new ElementSetter().beautifyLayout(this);
        }else {
            setContentView(R.layout.content_minesweeper);
        }



        // reference the Android Content(Textview,Imagebutton...)
        txtMineCount = (TextView) findViewById(R.id.MineCount);
        txtTimer = (TextView) findViewById(R.id.Timer);
        btnSmile = (ImageButton) findViewById(R.id.Smiley);

        SharedPreferences ledWallPrefs = getSharedPreferences(getString(R.string.shared_preferences_key), MODE_PRIVATE);
        String ip = ledWallPrefs.getString(getString(R.string.ip_preference_key), getString(R.string.default_ip));
        int port = (ledWallPrefs.getInt(getString(R.string.port_preference_key), getResources().getInteger(R.integer.default_port)));
        Log.d(TAG, ip);
        try {
            this.sender = new UdpSender(InetAddress.getByName(ip), port);
        } catch(Exception ex){
            this.sender = null;
            Log.e(TAG, "socket fail", ex);
        }

        // Set an event when a button is press
        btnSmile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                sender.sendModeChange(LedWallMode.MINESWEEPER);
                endGame();
                startGame();
            }
        });
        //This is the table on the android device where the Blocks will be put
        mineField = (TableLayout)findViewById(R.id.MineField);
        botLayout = (LinearLayout) findViewById(R.id.botLayout);
        //Short info how to start the game

        //Check screen resolution and edit the mine height
        final DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        //Get the correct heigth for the mine field
        mineField.post(new Runnable() {
            @Override
            public void run() {

                int  botHeight_px = botLayout.getHeight();
                double botHeight_dp = pxToDp(botHeight_px);

                height = botHeight_dp/numberOfRowsInMineField;
                double value;
                if(currentOrientation == Surface.ROTATION_0){
                    value = (botHeight_px-standardMinPortrait)/(standardMax-standardMinPortrait);
                }else {
                    value = (botHeight_px-getStandardMinLandscape)/(standardMax-getStandardMinLandscape);
                }
                height += difference*value;
                height = height - blockPadding*numberOfRowsInMineField;
                game = new MinesweeperGame(sender, MinesweeperActivity.this);

                showDialog("Press smiley to start New Game", 2000, true, false);
            }
        });
    }

    /**
     * Call the start method in logic
     */
    private void startGame()
    {
        game.startNewGame(MinesweeperActivity.this);
    }

    /**
     * Call the end method in logic
     * End the game xD
     * Reset the values on fields
     * send signal to reset LED-WALL
     */
    private void endGame()
    {
        game.endExistingGame(btnSmile, mineField, txtMineCount, txtTimer);
    }

    public void updateMineCountDisplay()
    {
        if (minesToFind < 0)
        {
            txtMineCount.setText(Integer.toString(minesToFind));
        }
        else if (minesToFind < 10)
        {
            txtMineCount.setText("00" + Integer.toString(minesToFind));
        }
        else if (minesToFind < 100)
        {
            txtMineCount.setText("0" + Integer.toString(minesToFind));
        }
        else
        {
            txtMineCount.setText(Integer.toString(minesToFind));
        }
    }

    public void setMines(int currentRow, int currentColumn)
    {
        // set mines excluding the location where user clicked
        Random rand = new Random();
        int mineRow, mineColumn;

        for (int mines = 0; mines < totalNumberOfMines; mines++)
        {
            mineRow = rand.nextInt(numberOfRowsInMineField)+1;
            mineColumn = rand.nextInt(numberOfColumnsInMineField)+1;
            if (!((mineRow >= currentRow-1 && mineRow <= currentRow+1 && mineColumn >= currentColumn-1 && mineColumn <= currentColumn+1) || blocks[mineRow][mineColumn].hasMine()))
            {
                blocks[mineRow][mineColumn].plantMine();
            }
            else
                mines--;
        }

        int nearByMineCount;

        // count number of mines in surrounding blocks
        for (int row = 0; row < numberOfRowsInMineField + 2; row++)
        {
            for (int column = 0; column < numberOfColumnsInMineField + 2; column++)
            {
                // for each block find nearby mine count
                nearByMineCount = 0;
                if ((row != 0) && (row != (numberOfRowsInMineField + 1)) && (column != 0) && (column != (numberOfColumnsInMineField + 1)))
                {
                    // check in all nearby blocks
                    for (int previousRow = -1; previousRow < 2; previousRow++)
                    {
                        for (int previousColumn = -1; previousColumn < 2; previousColumn++)
                        {
                            if (blocks[row + previousRow][column + previousColumn].hasMine())
                            {
                                // a mine was found so increment the counter
                                nearByMineCount++;
                            }
                        }
                    }
                    blocks[row][column].setNumberOfMinesInSurrounding(nearByMineCount);
                }
                // for side rows (0th and last row/column)
                // set count as 9 and mark it as opened
                else
                {
                    blocks[row][column].setNumberOfMinesInSurrounding(9);
                    blocks[row][column].OpenBlock();
                }
            }
        }
    }

    @SuppressWarnings("deprecation")
    public void rippleUncover(int rowClicked, int columnClicked, List<Block> blocksToSend)
    {
        // don't open flagged or mined rows
        if (blocks[rowClicked][columnClicked].hasMine() || blocks[rowClicked][columnClicked].isFlagged())
        {
            return;
        }

        if(blocks[rowClicked][columnClicked].isFlagged())
            blocksToSend.add(SimpleBlock.of(rowClicked, columnClicked, blocks[rowClicked][columnClicked]));

        // open clicked block
        blocks[rowClicked][columnClicked].OpenBlock();
        //WORKING!
        sender.sendBlock(new SimpleBlock(rowClicked, columnClicked, blocks[rowClicked][columnClicked].countMinesSurrounding()));

        // if clicked block have nearby mines then don't open further
        if (blocks[rowClicked][columnClicked].countMinesSurrounding() != 0 )
        {
            return;
        }

        // open next 3 rows and 3 columns recursively
        for (int row = 0; row < 3; row++)
        {
            for (int column = 0; column < 3; column++)
            {
                // check all the above checked conditions
                // if met then open subsequent blocks
                if (blocks[rowClicked + row - 1][columnClicked + column - 1].isCovered()
                        && (rowClicked + row - 1 > 0) && (columnClicked + column - 1 > 0)
                        && (rowClicked + row - 1 < numberOfRowsInMineField + 1) && (columnClicked + column - 1 < numberOfColumnsInMineField + 1))
                {
                    rippleUncover(rowClicked + row - 1, columnClicked + column - 1, blocksToSend );
                }
            }
        }
    }

    //region TIMER
    public void startTimer()
    {
        if (secondsPassed == 0)
        {
            timer.removeCallbacks(updateTimeElasped);
            // tell timer to run call back after 1 second
            timer.postDelayed(updateTimeElasped, 1000);
        }
    }

    public void stopTimer()
    {
        // disable call backs
        timer.removeCallbacks(updateTimeElasped);
    }
    //endregion

    public void showDialog(String message, int milliseconds, boolean useSmileImage, boolean useCoolImage)
    {
        // show message
        Toast dialog = Toast.makeText(
                getApplicationContext(),
                message,
                Toast.LENGTH_LONG);

        View dialogView = dialog.getView();
        dialog.setGravity(Gravity.CENTER, 0, 0);

        ImageView coolImage = new ImageView(getApplicationContext());
        if (useSmileImage)
        {
            coolImage.setImageResource(R.drawable.smile);
        }
        else if (useCoolImage)
        {
            coolImage.setImageResource(R.drawable.cool);
        }
        else
        {
            coolImage.setImageResource(R.drawable.sad);
        }
        if(dialogView instanceof LinearLayout){
            ((LinearLayout)dialogView).addView(coolImage,0);
        } else if(dialogView instanceof RelativeLayout){
            ((RelativeLayout)dialogView).addView(coolImage,0);
        }
        dialog.setDuration(milliseconds);
        dialog.show();
    }

    public double pxToDp(int px) {
        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        return (double) (px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

}