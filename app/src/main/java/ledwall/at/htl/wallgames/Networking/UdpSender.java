package ledwall.at.htl.wallgames.Networking;

import android.os.AsyncTask;
import android.util.Log;
import org.bitbucket.tuesd4y.ledwallapi.networking.MessageSender;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.charset.Charset;

/**
 * @author Christopher Stelzmüller
 * @version 1.0
 *          created on 23.12.2016
 */

public class UdpSender extends MessageSender {
    private final String TAG = getClass().getSimpleName();

    @Override
    protected void sendRawMessage(String message) {
            new SendMessageAsyncTask().execute(message);
    }

    public UdpSender(InetAddress ipAddress, int port) throws SocketException {
        super(ipAddress, port);
    }


    /**
     * AsyncTask which is used within the {@code sendRawMessage}-function to prevent the network-operation
     * from blocking the UI-thread. The Task returns true if every message in the params has been sent.
     */
    private class SendMessageAsyncTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... messages) {
            boolean allSent = true;
            for (String message : messages) {
                byte[] byteMessage = message.getBytes(Charset.forName("UTF-8"));
                try {
                    socket.send(new DatagramPacket(byteMessage, byteMessage.length, getIpAddress(), getPort()));
                    Log.d(TAG, message);
                } catch (IOException e) {
                    e.printStackTrace();
                    allSent = false;
                }
            }
            return allSent;
        }
    }
}
