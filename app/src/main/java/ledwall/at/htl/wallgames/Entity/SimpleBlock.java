package ledwall.at.htl.wallgames.Entity;

import org.bitbucket.tuesd4y.ledwallapi.entity.BaseBlock;
import org.bitbucket.tuesd4y.ledwallapi.entity.Block;

/**
 * Provides an interface to convert a MinesweeperBlock to a BaseBlock
 * @author stoez
 * @version 1.1 6/3/2016
 *
 * @deprecated TODO: replace SimpleBlock by implementing the Block-interface in MineSweeperBlock
 */
public class SimpleBlock extends BaseBlock {
    private int row;
    private int col;
    private int colour;

    public SimpleBlock(int row, int col, int colour) {
        this.row = row;
        this.col = col;
        this.colour = colour;
    }

    @Override
    public int getRow() {
        return row;
    }

    @Override
    public int getColumn() {
        return col;
    }

    @Override
    public int getType() {
        return colour;
    }

    /**
     * Creates a block from the data stored in a regular MinesweeperBlock
     * @param row   the row the block is in
     * @param col   the column the block is in
     * @param block the block itself
     * @return
     *      a new Block representing the given MinesweeperBlock which can be sent to the LedWall
     */
    public static Block of(int row, int col, MinesweeperBlock block) {
        int colour = block.countMinesSurrounding();
        if(block.isFlagged()) {
            colour = 9;
        } else if(block.isQuestionMarked()) {
            colour = 10;
        } else if( block.getText().toString().equals("M")){
            colour = 11;
        } else if(block.isCovered()){
            colour = 12;
        }
        //noinspection deprecation
        return new SimpleBlock(row, col, colour);
    }
}
