package ledwall.at.htl.wallgames.Business;

/**
 * A callback which is executed every time the snake eats some food
 * @author stoez
 * @version 1.0 3/10/2016
 */
public interface ScoreUpdateCallback {
    void callOnScoreUpdate(ScoreCounter counter);
}
