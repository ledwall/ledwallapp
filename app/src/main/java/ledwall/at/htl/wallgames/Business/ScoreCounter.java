package ledwall.at.htl.wallgames.Business;

/**
 * A simple counter which is used to count the score in the SnakeGame
 * @author stoez
 * @version 1.0 3/10/2016
 */
public class ScoreCounter {
    Integer score;

    public ScoreCounter() {
        this.score = 0;
    }

    public void inc(){
        this.score++;
    }

    public int getScore(){
        return score;
    }
}
