package ledwall.at.htl.wallgames;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;

import ledwall.at.htl.wallgames.Business.ElementSetter;
import ledwall.at.htl.wallgames.Entity.SnakeScore;
import ledwall.at.htl.wallgames.Networking.PostScoreAsyncTask;

/**
 * @author Tanzer, stoez
 * @version 5/23/2016
 */
public class HomeActivity extends MenuActivity {
    private String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_home, null, false);
        mDrawer.addView(contentView, 1);

        new ElementSetter().beautifyLayout(this);

        SharedPreferences highscorePrefs = getSharedPreferences(getString(R.string.highscores_shared_preference_key), Context.MODE_PRIVATE);
        if (highscorePrefs.contains(getString(R.string.highscore_object_preference_key))) {
            Log.d(TAG, "onCreate: trying to send the score again");
            try {
                SnakeScore score = new SnakeScore(new JSONObject(highscorePrefs.getString(getString(R.string.highscore_object_preference_key), "")));
                new PostScoreAsyncTask(getApplicationContext(), score).execute();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
