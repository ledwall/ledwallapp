package ledwall.at.htl.wallgames.Entity;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import ledwall.at.htl.wallgames.R;

/**
 * A block that is used in MinesweeperLogic which is not implementing the Block interface.
 * A SimpleBlock can be created from a MinesweeperBlock to send it to the LedWall
 * @author Mosquera, minor changes by Tanzer, stoez
 * @version 27/5/2016
 */
public class MinesweeperBlock extends Button
{
    private boolean isCovered; // is block covered yet
    private boolean isMined; // does the block has a mine underneath
    private boolean isFlagged; // is block flagged as a potential mine
    private boolean isQuestionMarked; // is block question marked
    private boolean isClickable; // can block accept click events
    private int numberOfMinesInSurrounding; // number of mines in nearby blocks

    public MinesweeperBlock(Context context)
    {
        super(context);
    }

    public MinesweeperBlock(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public MinesweeperBlock(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
    }

    /**
     * Sets de default properties of the block
     */
    public void setDefaults()
    {
        isCovered = true;
        isMined = false;
        isFlagged = false;
        isQuestionMarked = false;
        isClickable = true;
        numberOfMinesInSurrounding = 0;

        this.setBackgroundResource(R.drawable.square_pink);
        setBoldFont();
    }

    /**
     * mark the block as opened and set the number of surrounding mines
     * @param number
     *      the number of surrounding mines
     */
    public void setNumberOfSurroundingMines(int number)
    {
        this.setBackgroundResource(R.drawable.square_blue);

        updateNumber(number);
    }


    /**
     * Mark as a mine and enable or disable the blog
     * @param enabled
     *      indicates if the block should be en- or disabled
     */
    public void setMineIcon(boolean enabled)
    {
        this.setText("M");

        if (!enabled)
        {
            this.setBackgroundResource(R.drawable.square_blue);
            this.setTextColor(Color.RED);
        }
        else
        {
            this.setTextColor(Color.BLACK);
        }
    }

    /**
     * Mark as flagged and enable or disable the block
     * @param enabled
     *      indicates if the block should be en- or disabled
     */
    public void setFlagIcon(boolean enabled)
    {
        this.setText("F");

        if (!enabled)
        {
            this.setBackgroundResource(R.drawable.square_pink);
            this.setTextColor(Color.RED);
        }
        else
        {
            this.setTextColor(Color.BLACK);
        }
    }

    /**
     * Mark as QuestionMarked and enable or disable the blog
     * @param enabled
     *      indicates if the block should be en- or disabled
     */
    public void setQuestionMarkIcon(boolean enabled)
    {
        this.setText("?");

        if (!enabled)
        {
            this.setBackgroundResource(R.drawable.square_blue);
            this.setTextColor(Color.RED);
        }
        else
        {
            this.setTextColor(Color.BLACK);
        }
    }

    /**
     * Set the block as enabled or disabled
     * @param enabled
     *      indicates if the block should be en- or disabled
     */
    public void setBlockAsDisabled(boolean enabled)
    {
        if (!enabled)
        {
            this.setBackgroundResource(R.drawable.square_blue);
        }
        else
        {
            this.setBackgroundResource(R.drawable.square_pink);
        }
    }

    public void clearAllIcons()
    {
        this.setText("");
    }

    private void setBoldFont()
    {
        this.setTypeface(null, Typeface.BOLD);
    }

    public void OpenBlock()
    {
        if (!isCovered)
            return;

        setBlockAsDisabled(false);
        isCovered = false;

        if (hasMine())  setMineIcon(false);
        else            setNumberOfSurroundingMines(numberOfMinesInSurrounding);
    }

    public void updateNumber(int text)
    {
        if (text != 0)
        {
            this.setText(Integer.toString(text));

            switch (text)
            {
                case 1:
                    this.setTextColor(Color.BLUE);
                    break;
                case 2:
                    this.setTextColor(Color.rgb(0, 100, 0));
                    break;
                case 3:
                    this.setTextColor(Color.RED);
                    break;
                case 4:
                    this.setTextColor(Color.rgb(85, 26, 139));
                    break;
                case 5:
                    this.setTextColor(Color.rgb(139, 28, 98));
                    break;
                case 6:
                    this.setTextColor(Color.rgb(238, 173, 14));
                    break;
                case 7:
                    this.setTextColor(Color.rgb(47, 79, 79));
                    break;
                case 8:
                    this.setTextColor(Color.rgb(71, 71, 71));
                    break;
                case 9:
                    this.setTextColor(Color.rgb(205, 205, 0));
                    break;
            }
        }
    }

    public void plantMine()
    {
        isMined = true;
    }

    public void triggerMine()
    {
        setMineIcon(true);
        this.setTextColor(Color.RED);
    }

    public boolean isCovered()
    {
        return isCovered;
    }

    public boolean hasMine()
    {
        return isMined;
    }

    public void setNumberOfMinesInSurrounding(int number)
    {
        numberOfMinesInSurrounding = number;
    }

    public int countMinesSurrounding()
    {
        return numberOfMinesInSurrounding;
    }

    public boolean isFlagged()
    {
        return isFlagged;
    }

    public void setFlagged(boolean flagged)
    {
        isFlagged = flagged;
    }

    public boolean isQuestionMarked()
    {
        return isQuestionMarked;
    }

    public void setQuestionMarked(boolean questionMarked)
    {
        isQuestionMarked = questionMarked;
    }

    public boolean isClickable()
    {
        return isClickable;
    }

    public void setClickable(boolean clickable)
    {
        isClickable = clickable;
    }
}

