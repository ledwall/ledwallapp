package ledwall.at.htl.wallgames.Logic;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.view.View;
import ledwall.at.htl.wallgames.Business.ScoreCounter;
import ledwall.at.htl.wallgames.Business.ScoreUpdateCallback;
import ledwall.at.htl.wallgames.SnakeActivity;
import org.bitbucket.tuesd4y.ledwallapi.entity.BaseBlock;
import org.bitbucket.tuesd4y.ledwallapi.entity.Block;
import org.bitbucket.tuesd4y.ledwallapi.entity.LedWallMode;
import org.bitbucket.tuesd4y.ledwallapi.entity.SpecialSignal;
import org.bitbucket.tuesd4y.ledwallapi.networking.MessageSender;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Playing field for snake
 * @author stoez
 * @version 5/23/2016
 */
public class SnakeGame extends View {
    private int mPadding = 0;
    public Snake snake;
    public boolean gameOver = false;
    private ScoreUpdateCallback mOnScoreUpdate;
    private boolean mSetupComplete = false;
    private int mPxSquare, mSquaresWidth, mSquaresHeight, mSqBorder = 0;
    private ArrayList<SnakeBlock> mWalls;
    private Food mFood;
    private Random mRandom;
    private ScoreCounter mScoreCounter;
    private SnakeActivity mActivity;
    private int mFrameRate;
    private MessageSender mSender;

    public void setOnScoreUpdate(ScoreUpdateCallback mOnScoreUpdate) {
        this.mOnScoreUpdate = mOnScoreUpdate;
    }

    public SnakeGame(SnakeActivity activity, int speed, MessageSender messageSender) {
        super(activity);
        mActivity = activity;
        mRandom = new Random();
        this.mScoreCounter = new ScoreCounter();
        this.mFrameRate = 4 * (speed + 1);
        this.mSender = messageSender;

        mOnScoreUpdate = null;
    }

    /**
     * gets the current score
     * @return
     *      retrieves the current score from the ScoreCounter object
     */
    public int getScore() {
        if (mScoreCounter == null)
            return 0;
        return mScoreCounter.getScore();
    }

    /**
     * Draws the playing field
     * @param canvas
     *      space provided to draw upon
     */
    @SuppressLint("DrawAllocation")
    protected void onDraw(Canvas canvas) {
        if (!mSetupComplete) {
            setup(mSender, true);
            this.invalidate();
            return;
        }

        for (SnakeBlock block : mWalls) {
            block.draw(canvas);
        }

        snake.draw(canvas);

        mFood.draw(canvas);

        final View parent = this;
        if (!snake.stopped) {
            new Thread(new Runnable() {
                public void run() {
                    parent.postDelayed(new Runnable() {
                        public void run() {
                            parent.invalidate();
                        }
                    }, 1000 / mFrameRate);
                }
            }).start();
        } else if (gameOver) {
            new Thread(new Runnable() {
                public void run() {
                    parent.postDelayed(new Runnable() {
                        public void run() {
                            mActivity.gameOver();
                        }
                    }, 500);
                }
            }).start();
        }
    }

    /**
     * sets up view for playing
     * @param messageSender
     *      MessageSender that will be used to send movements of the snake to the LedWall
     * @param startUp
     *      indicates if the game should be started straight after setup is completed
     * @return
     *      padding of each blocks
     */
    public int setup(MessageSender messageSender, Boolean startUp) {
        this.mScoreCounter = new ScoreCounter();
        if (mOnScoreUpdate != null)  mOnScoreUpdate.callOnScoreUpdate(mScoreCounter);

        gameOver = false;

        //region calculating block sizes and paddings
        int pxWidth = getWidth();
        int pxHeight = getHeight();

        mSquaresHeight = 7 + 2;
        mSquaresWidth = 14 + 2;

        int pxSquareWidth = pxWidth / mSquaresWidth;
        int pxSquareHeight = pxHeight / mSquaresHeight;
        if (pxSquareWidth > pxSquareHeight) mPxSquare = pxSquareHeight;
        else mPxSquare = pxSquareWidth;

        mPadding = (pxWidth - mSquaresWidth * mPxSquare) / 2;
        //endregion

        mWalls = new ArrayList<>();
        for (int j = 0; j < mSquaresWidth; j++) {
            mWalls.add(new SnakeBlock(j, 0, 0));
            mWalls.add(new SnakeBlock(j, mSquaresHeight - 1, 0));
        }
        for (int j = 1; j < (mSquaresHeight - 1); j++) {
            mWalls.add(new SnakeBlock(0, j, 0));
            mWalls.add(new SnakeBlock(mSquaresWidth - 1, j, 0));
        }

        if (messageSender != null)  messageSender.sendModeChange(LedWallMode.GAME);

        snake = new Snake(messageSender);

        if (startUp)    snake.stopped = true;

        mFood = new Food(snake, mWalls);

        mSetupComplete = true;

        return mPadding;
    }

    /**
     * Snake class, representing the snake moving around on the gameboard, stores an array of blocks
     * and the direction the snake is currently moving
     */
    public class Snake {
        public ArrayList<SnakeBlock> blocks;
        public boolean stopped = false;
        public MessageSender sender = null;
        private Block lastBlock = null;
        private int direction, length, lastdirection;

        public Snake(MessageSender sender) {

            this.sender = sender;
            blocks = new ArrayList<>();
            blocks.add(new SnakeBlock(mSquaresWidth / 2, mSquaresHeight / 2, 1));
            length = 3;

            //calculate random direction; either right or left
            direction = mRandom.nextInt(2);
            if(direction == 0) {

                blocks.add(new SnakeBlock(mSquaresWidth / 2 - 1, mSquaresHeight / 2, 1));
                blocks.add(new SnakeBlock(mSquaresWidth / 2 - 2, mSquaresHeight / 2, 1));
            } else{
                direction = 2;
                blocks.add(new SnakeBlock(mSquaresWidth / 2 + 1, mSquaresHeight / 2, 1));
                blocks.add(new SnakeBlock(mSquaresWidth / 2 + 2, mSquaresHeight / 2, 1));
            }
            lastdirection = direction;

            List<Block> blocksToSend = new LinkedList<>();
            for (Block b : blocks)  blocksToSend.add(b);
            if (sender != null) sender.sendBlocks(blocksToSend);
        }

        /**
         * moves the snake one step and draws the new position
         * @param canvas
         *      object to draw on
         */
        public void draw(Canvas canvas) {
            if (!stopped) move();
            List<Block> b = new LinkedList<>();
            for (SnakeBlock block : blocks) block.draw(canvas);
            if (blocks.get(0).getType() == 3) {
                b.addAll(blocks);
            } else {
                b.add(blocks.get(0));
                b.add(blocks.get(blocks.size() - 1));
            }

            if (lastBlock != null) {
                ((SnakeBlock) lastBlock).setType(0);
                b.add(lastBlock);
            }
            if (sender != null) {
                if (gameOver)   sender.sendSpecialSignal(SpecialSignal.SNAKE_IS_DEAD);
                else            sender.sendBlocks(b);
            } else {
                throw new NullPointerException("The Sender is null and therefore cannot send data...");
            }
        }

        //region direction change methods for portrait direction buttons
        public void turnLeft() {
            if (this.lastdirection != 0 && this.lastdirection != 2)
                this.direction = 2;
        }

        public void turnRight() {
            if (this.lastdirection != 0 && this.lastdirection != 2)
                this.direction = 0;
        }

        public void turnDown() {
            if (this.lastdirection != 1 && this.lastdirection != 3)
                this.direction = 1;
        }

        public void turnUp() {
            if (this.lastdirection != 1 && this.lastdirection != 3)
                this.direction = 3;
        }
        //endregion

        //region direction change methods for view tap and volume buttons
        public void upPress() {
            this.direction = lastdirection + 1;
            if (this.direction > 3) this.direction = 0;
        }

        public void downPress() {
            this.direction = lastdirection - 1;
            if (this.direction < 0) this.direction = 3;
        }
        //endregion

        /**
         * Moves the snake one space into its stored direction
         */
        public void move() {
            SnakeBlock frontBlock = blocks.get(0);

            SnakeBlock newBlock;
            lastdirection = direction;
            switch (direction) {
                case 0:
                    newBlock = new SnakeBlock(frontBlock.x + 1, frontBlock.y, 1);
                    break;
                case 1:
                    newBlock = new SnakeBlock(frontBlock.x, frontBlock.y + 1, 1);
                    break;
                case 2:
                    newBlock = new SnakeBlock(frontBlock.x - 1, frontBlock.y, 1);
                    break;
                default:
                    newBlock = new SnakeBlock(frontBlock.x, frontBlock.y - 1, 1);
            }

            //check if wall has been hit
            if (this.collides(newBlock) || newBlock.collides(mWalls)) {
                stopped = true;
                for (SnakeBlock block : blocks) {
                    block.setType(3);
                }
                newBlock.setType(0);
                gameOver = true;

            } else {
                blocks.add(0, newBlock);

                if (this.collides(mFood)) {
                    mFood.move(this, mWalls);
                    length++;
                    mScoreCounter.inc();
                    if (mOnScoreUpdate != null) {
                        mOnScoreUpdate.callOnScoreUpdate(mScoreCounter);
                    }
                    lastBlock = null;
                } else {
                    lastBlock = blocks.get(length);
                    blocks.remove(length);
                }
            }
        }

        /**
         * check if the snake collides to another block
         * @param block
         *      the first block of the snake
         * @return
         *      true if collision, otherwise false
         */
        public boolean collides(SnakeBlock block) {
            for (SnakeBlock oneBlock : this.blocks)
                if (block.collides(oneBlock)) return true;
            return false;
        }
    }

    /**
     * One block in the snake game field, can be easily sent with an MessageSender
     */
    public class SnakeBlock extends BaseBlock implements Block {
        public int x = 0, y = 0;
        ShapeDrawable shape;
        private int type = -1;

        public SnakeBlock() {
        }

        public SnakeBlock(int x, int y, int type) {
            this.x = x;
            this.y = y;
            shape = new ShapeDrawable(new RectShape());
            shape.setBounds(mPadding + x * mPxSquare + mSqBorder, y * mPxSquare + mSqBorder,
                    mPadding + (x + 1) * mPxSquare - mSqBorder, (y + 1) * mPxSquare - mSqBorder);
            this.setType(type);
        }

        @Override
        public int getRow() {
            return y;
        }

        @Override
        public int getColumn() {
            return x;
        }

        public void draw(Canvas canvas) {
            shape.draw(canvas);
        }

        public boolean collides(SnakeBlock block) {
            return block.x == this.x && block.y == this.y;
        }

        public boolean collides(ArrayList<SnakeBlock> blocks) {
            for (SnakeBlock block : blocks) {
                if (this.collides(block)) return true;
            }
            return false;
        }

        @Override
        public int getType() {
            return this.type;
        }

        public void setType(int type) {
            switch (type) {
                case 0: //If Wall, Paint Black
                    shape.getPaint().setColor(Color.BLACK);
                    break;
                case 1: //If Snake, Paint Blue
                    shape.getPaint().setColor(Color.parseColor("#ff33b5e5"));
                    break;
                case 2: //If Food, Paint Grey
                    shape.getPaint().setColor(Color.GRAY);
                    break;
                case 3: //If Collision, Paint Red
                    shape.getPaint().setColor(Color.RED);
            }
            this.type = type;
        }
    }

    /**
     * Food class, edible by the snake, unlike walls
     */
    class Food extends SnakeBlock {

        public Food(Snake snake, ArrayList<SnakeBlock> blocks) {
            shape = new ShapeDrawable(new RectShape());
            this.setType(2);
            this.move(snake, blocks);
        }

        public void move(Snake snake, ArrayList<SnakeBlock> blocks) {
            while (true) {
                this.x = mRandom.nextInt(mSquaresWidth - 3) + 1;
                this.y = mRandom.nextInt(mSquaresHeight - 3) + 1;
                if (!snake.collides(this) && !this.collides(blocks)) break;
            }
            snake.sender.sendBlock(this);
            shape.setBounds(mPadding + x * mPxSquare + mSqBorder, y * mPxSquare + mSqBorder,
                    mPadding + (x + 1) * mPxSquare - mSqBorder, (y + 1) * mPxSquare - mSqBorder);
        }

    }
}
