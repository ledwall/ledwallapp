package ledwall.at.htl.wallgames;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ListView;
import android.widget.RelativeLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ledwall.at.htl.wallgames.Business.ElementSetter;
import ledwall.at.htl.wallgames.Business.SimpleDividerItemDecoration;
import ledwall.at.htl.wallgames.Business.SnakeScoreAdapter;
import ledwall.at.htl.wallgames.Entity.SnakeScore;

/**
 * @author stoez
 * @version 5/22/2016
 */
public class DisplaySnakeScoresActivity extends MenuActivity {
    private final String TAG = getClass().getSimpleName();
    private List<SnakeScore> mScoreList = new ArrayList<>();
    private SnakeScoreAdapter mSnakeScoreAdapter;

    private RecyclerView mScoreRecyclerView;
    private RelativeLayout mRetryLayout;
    private RelativeLayout mLoadingLayout;
    private SharedPreferences mLedWallPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_display_snake_scores, null, false);
        mDrawer.addView(contentView, 1);
        useJustOneColor();
        new ElementSetter().beautifyLayout(this);

        //region fetching view references
        mScoreRecyclerView = (RecyclerView) findViewById(R.id.scoreView);
        mLoadingLayout = (RelativeLayout) findViewById(R.id.loadingLayout);
        mRetryLayout = (RelativeLayout) findViewById(R.id.retryLayout);
        FloatingActionButton retryButton = (FloatingActionButton) findViewById(R.id.retryButton);
        //endregion

        //region setting up score list
        mScoreRecyclerView.setVisibility(View.GONE);
        mRetryLayout.setVisibility(View.GONE);
        mLoadingLayout.setVisibility(View.VISIBLE);
        assert mScoreRecyclerView != null;
        mScoreRecyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mScoreRecyclerView.setLayoutManager(llm);
        mSnakeScoreAdapter = new SnakeScoreAdapter(mScoreList);
        mScoreRecyclerView.setAdapter(mSnakeScoreAdapter);
        mScoreRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getResources()));
        //endregion

        assert retryButton != null;
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation fadeOut = new AlphaAnimation(1,0);
                fadeOut.setInterpolator(new DecelerateInterpolator());
                fadeOut.setDuration(500);

                Animation fadeIn = new AlphaAnimation(0,1);
                fadeIn.setInterpolator(new AccelerateInterpolator());
                fadeIn.setDuration(500);

                new GetScoresAsyncTask().execute();

                mRetryLayout.setAnimation(fadeOut);
                fadeOut.startNow();
                mRetryLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mRetryLayout.setVisibility(View.INVISIBLE);
                    }
                }, fadeOut.getDuration());

                mLoadingLayout.setAnimation(fadeIn);
                fadeIn.startNow();
                mLoadingLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mLoadingLayout.setVisibility(View.VISIBLE);
                    }
                }, fadeIn.getDuration());
            }
        });
        new GetScoresAsyncTask().execute();

        mLedWallPrefs = getSharedPreferences(
                getString(R.string.shared_preferences_key), MODE_PRIVATE);
    }

    private class GetScoresAsyncTask extends AsyncTask<Void, Void, JSONArray>{

        @Override
        protected JSONArray doInBackground(Void... params) {
            URL url;
            HttpURLConnection client = null;
            try {
                url = new URL(String.format(getString(R.string.score_url_parse_string), getString(R.string.score_server_ip)) + "/forApp");
                client = (HttpURLConnection) url.openConnection();

                client.setRequestMethod("GET");
                client.setRequestProperty("Content-Type", "application/json");
                client.setConnectTimeout(2500);
                client.setReadTimeout(1000);

                int res = client.getResponseCode();
                if(res == HttpURLConnection.HTTP_OK){
                    StringBuilder builder = new StringBuilder();

                    try(BufferedReader reader = new BufferedReader(
                            new InputStreamReader(client.getInputStream()))){
                        String line;
                        while((line = reader.readLine()) != null){
                            builder.append(line);
                        }
                    }
                    catch(IOException e){
                        Log.e(TAG, "some random exception..", e);
                    }

                    return new JSONArray(builder.toString());
                }

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            } finally {
                if(client != null){
                    client.disconnect();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONArray scores) {
            Animation fadeOut = new AlphaAnimation(1,0);
            fadeOut.setInterpolator(new DecelerateInterpolator());
            fadeOut.setDuration(500);

            Animation fadeIn = new AlphaAnimation(0,1);
            fadeIn.setInterpolator(new AccelerateInterpolator());
            fadeIn.setDuration(500);

            mLoadingLayout.setAnimation(fadeOut);
            fadeOut.startNow();
            mLoadingLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mLoadingLayout.setVisibility(View.INVISIBLE);
                }
            }, fadeOut.getDuration());

            if(scores != null) {
                mScoreRecyclerView.setAnimation(fadeIn);
                fadeIn.startNow();
                mScoreRecyclerView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mScoreRecyclerView.setVisibility(View.VISIBLE);
                    }
                }, fadeIn.getDuration());
                mScoreRecyclerView.setVisibility(View.VISIBLE);
                convertJsonToList(scores);
                mSnakeScoreAdapter.notifyDataSetChanged();
                mScoreRecyclerView.smoothScrollToPosition(0);
            }
            if(scores == null || mScoreList.size() == 0){
                //display no scores found message, show retry button
                //onclick of retry button should be set already
                mRetryLayout.setAnimation(fadeIn);
                fadeIn.startNow();
                mRetryLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mRetryLayout.setVisibility(View.VISIBLE);
                    }
                }, fadeIn.getDuration());
            }
        }
    }

    private void convertJsonToList(JSONArray list) {
        mScoreList.clear();

        try {
            for (int i = 0; i < list.length(); i++) {
                JSONObject jsonScore = list.getJSONObject(i);

                mScoreList.add(new SnakeScore(jsonScore));
            }
        } catch (JSONException e) {
            Log.e(TAG, "", e);
        }

        Collections.sort(mScoreList, new Comparator<SnakeScore>() {
            @Override
            public int compare(SnakeScore lhs, SnakeScore rhs) {
                return Integer.compare(rhs.getScore(), lhs.getScore());
            }
        });


        int highscore = mLedWallPrefs.getInt(getString(R.string.snake_highscore_preference_key), 0);
        SnakeScore yourHighScore = new SnakeScore(highscore, null, null);
        mScoreList.add(0,yourHighScore);
    }
}
