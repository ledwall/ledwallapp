package ledwall.at.htl.wallgames.Business;

/**
 * A callback that is executed when the ConnectFourGame
 * @author stoez
 * @version 1.0 3/24/2016
 */
public interface OnConnectFourWinCallback {
    void callOnWin();
}
