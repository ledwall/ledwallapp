package ledwall.at.htl.wallgames;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;
import ledwall.at.htl.wallgames.Business.ElementSetter;
import ledwall.at.htl.wallgames.Business.OnConnectFourWinCallback;
import ledwall.at.htl.wallgames.Entity.ConnectFourBlock;
import ledwall.at.htl.wallgames.Logic.ConnectFourGame;
import ledwall.at.htl.wallgames.Networking.UdpSender;
import org.bitbucket.tuesd4y.ledwallapi.entity.SpecialSignal;
import org.bitbucket.tuesd4y.ledwallapi.networking.MessageSender;

import java.net.InetAddress;
import java.util.Locale;

/**
 * @author Mosquera, stoez, Tanzer
 * @version  5/19/2016
 */
public class ConnectFourActivity extends MenuActivity {
    private final String TAG = getClass().getSimpleName();

    private MessageSender mSender;
    private ConnectFourGame mGame;
    private TableLayout mConnectFourTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Display display = ((WindowManager)getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
        final int currentOrientation = display.getRotation();

        if(currentOrientation == Surface.ROTATION_0){
            LayoutInflater inflater = (LayoutInflater) this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View contentView = inflater.inflate(R.layout.activity_connect_four, null, false);
            mDrawer.addView(contentView, 1);

            ImageView resetButton = (ImageView) findViewById(R.id.btn_restart);

            resetButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mGame.resetGame();
                }
            });

            new ElementSetter().beautifyLayout(this);

        }else {
            setContentView(R.layout.content_connect_four);
        }

        //region setup game and udp sender
        mConnectFourTable = (TableLayout) findViewById(R.id.tl_connectFourField);
        mGame = new ConnectFourGame(getApplicationContext());
        SharedPreferences ledWallPrefs = getSharedPreferences(
                getString(R.string.shared_preferences_key), MODE_PRIVATE);

        String ip = ledWallPrefs.getString(
                getString(R.string.ip_preference_key),
                getString(R.string.default_ip));
        int port = (ledWallPrefs.getInt(
                getString(R.string.port_preference_key),
                getResources().getInteger(R.integer.default_port)));
        try {
            mSender = new UdpSender(InetAddress.getByName(ip), port);
            mGame.setSender(mSender);
            mGame.resetGame();
        } catch(Exception ex){
            mSender = null;
            Log.e(TAG, "Couldn't create Udp-Sender", ex);
        }

        mConnectFourTable.post(new Runnable() {
            @Override
            public void run() {
                int rowWidth = mConnectFourTable.getWidth();
                Log.i(TAG, "Connect4Table Height: " + String.valueOf(mConnectFourTable.getHeight()));
                int rowHeight = (mConnectFourTable.getHeight()/ ConnectFourGame.ROWS);
                int blockWidth = rowWidth/ ConnectFourGame.COLUMNS;

                TableLayout.LayoutParams rowLp = new TableLayout.LayoutParams(
                        rowWidth,
                        rowHeight,
                        1.0f);
                TableRow.LayoutParams cellLp = new TableRow.LayoutParams(
                        blockWidth,
                        rowHeight,
                        1.0f);
                for (int r = 0; r < ConnectFourGame.ROWS; ++r)
                {
                    TableRow row = new TableRow(ConnectFourActivity.this);
                    for (int c = 0; c < ConnectFourGame.COLUMNS; ++c)
                    {
                        final int col = c;
                        ConnectFourBlock btn = mGame.table[r][c];
                        btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(!mGame.isOver()) {
                                    if (mGame.dropTokenInColumn(col))
                                        Log.d(TAG, getString(R.string.token_dropped_log_text) + col);
                                }
                                else{
                                    if(currentOrientation == Surface.ROTATION_90 || currentOrientation == Surface.ROTATION_270){
                                        mGame.resetGame();
                                        Toast.makeText(
                                                getApplicationContext(),
                                                R.string.game_restarted_text,
                                                Toast.LENGTH_LONG)
                                                .show();
                                    }
                                }
                            }
                        });
                        row.addView(btn, cellLp);
                    }
                    mConnectFourTable.addView(row, rowLp);
                }
            }
        });

        mGame.setOnWinCallback(new OnConnectFourWinCallback() {
            @Override
            public void callOnWin(){
                Log.d(TAG,
                        String.format(Locale.GERMAN, getString(R.string.game_won_portrait_format), mGame.getCurrentPlayer()));
                if(currentOrientation == Surface.ROTATION_0){
                    Toast.makeText(
                            getApplicationContext(),
                            String.format(Locale.GERMAN, getString(R.string.game_won_portrait_format), mGame.getCurrentPlayer()),
                            Toast.LENGTH_LONG)
                            .show();
                }
                else{
                    Toast.makeText(
                            getApplicationContext(),
                            String.format(Locale.GERMAN, getString(R.string.game_won_landscape_format), mGame.getCurrentPlayer()),
                            Toast.LENGTH_LONG)
                            .show();
                }
                mConnectFourTable.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mSender.sendSpecialSignal(SpecialSignal.CONNECT_4_IS_OVER);
                    }
                }, 100);
            }
        });
        //endregion
    }

    public ConnectFourGame getGame(){
        return mGame;
    }
}
