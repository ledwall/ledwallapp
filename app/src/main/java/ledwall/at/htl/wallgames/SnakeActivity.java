package ledwall.at.htl.wallgames;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import ledwall.at.htl.wallgames.Business.ElementSetter;
import ledwall.at.htl.wallgames.Business.ScoreCounter;
import ledwall.at.htl.wallgames.Business.ScoreUpdateCallback;
import ledwall.at.htl.wallgames.Business.SnakeEndDialog;
import ledwall.at.htl.wallgames.Logic.SnakeGame;
import ledwall.at.htl.wallgames.Networking.UdpSender;
import org.bitbucket.tuesd4y.ledwallapi.networking.MessageSender;

import java.net.InetAddress;
import java.util.Locale;

/**
 * @author stoez
 * @version 5/30/2016
 */
public class SnakeActivity extends MenuActivity {
    private final String TAG = getClass().getSimpleName();

    private FrameLayout mLeft;
    private FrameLayout mRight;
    private LinearLayout mMidButtons;
    private LinearLayout mTopLayout;
    private SharedPreferences mLedWallPrefs;
    private TextView mCountDown;
    private int mGameFramePadding = 0;
    private SnakeGame mGame;
    private FrameLayout mGameFrame;
    private MessageSender mSender;

    public SharedPreferences getLedWallPrefs() {
        return mLedWallPrefs;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        SnakeEndDialog.Companion.setOpen(false );
        super.onCreate(savedInstanceState);

        //region set up layout, both portrait and landscape orientation
        Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
        int currentOrientation = display.getRotation();
        Log.i(TAG, "The current rotation is: " + currentOrientation);

        if (currentOrientation == Surface.ROTATION_0) {
            LayoutInflater inflater = (LayoutInflater) this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams")
            View contentView = inflater.inflate(R.layout.activity_snake, null, false);
            mDrawer.addView(contentView, 1);
        } else {
            setContentView(R.layout.content_snake);
        }
        //endregion

        mLedWallPrefs = getSharedPreferences(
                getString(R.string.shared_preferences_key), MODE_PRIVATE);

        setUpUdpSender(mLedWallPrefs);

        // region set buttons for portrait layout and beautify
        if (currentOrientation == Surface.ROTATION_0) {
            FrameLayout up = (FrameLayout) findViewById(R.id.fl_up);
            FrameLayout down = (FrameLayout) findViewById(R.id.fl_down);
            mLeft = (FrameLayout) findViewById(R.id.fl_left);
            mRight = (FrameLayout) findViewById(R.id.fl_right);
            addButtonClickListener(up, down, mLeft, mRight);

            new ElementSetter().beautifyLayout(this);
        }
        //endregion

        //region fetching references from view
        mMidButtons = (LinearLayout) findViewById(R.id.ll_sideButtons);
        final ImageButton ibLeft = (ImageButton) findViewById(R.id.ib_left);
        final ImageButton ibRight = (ImageButton) findViewById(R.id.ib_right);
        final TextView tv_score = (TextView) findViewById(R.id.tv_score);
        final TextView tv_highScore = (TextView) findViewById(R.id.tv_highScore);
        mCountDown = (TextView) findViewById(R.id.countDown);
        mGameFrame = (FrameLayout) findViewById(R.id.gameFrame);
        //endregion

        mCountDown.setVisibility(View.INVISIBLE);

        mGame = new SnakeGame(this, getResources().getInteger(R.integer.default_speed), mSender);
        mGameFrame.addView(mGame);

        //region setScoreTextView + Layout
        if (currentOrientation == Surface.ROTATION_0) {
            tv_score.post(new Runnable() {
                @Override
                public void run() {
                    LinearLayout botLayout = (LinearLayout) findViewById(R.id.botLayout);
                    tv_score.getLayoutParams().width = botLayout.getWidth() / 2;
                    tv_highScore.getLayoutParams().width = botLayout.getWidth() / 2;

                    tv_highScore.setPadding(mGameFramePadding, 0, 0, 0);

                    tv_score.requestLayout();
                    tv_highScore.requestLayout();
                }
            });

            mMidButtons.post(new Runnable() {
                @Override
                public void run() {
                    int width = mMidButtons.getWidth();
                    mLeft.getLayoutParams().width = width / 2;
                    mRight.getLayoutParams().width = width / 2;

                    mLeft.requestLayout();
                    mRight.requestLayout();
                }
            });

        } else {
            final FrameLayout fl = (FrameLayout) findViewById(R.id.fl);
            fl.post(new Runnable() {
                @Override
                public void run() {
                    mTopLayout = (LinearLayout) findViewById(R.id.topLayout);
                    fl.getLayoutParams().height = mTopLayout.getHeight() - tv_score.getHeight() - 50;
                    fl.requestLayout();

                    mGameFrame.getLayoutParams().height = fl.getHeight() - 50;
                    mGameFrame.getLayoutParams().width = fl.getWidth() - 150;
                    mGameFrame.requestLayout();

                    tv_highScore.setPadding(140, 0, 75, 0);
                    tv_score.setPadding(75, 0, 140, 0);
                }
            });
        }
        //endregion

        tv_highScore.setText(
                String.format(
                        Locale.GERMAN,
                        "Highscore: %d",
                        mLedWallPrefs.getInt(getString(
                                R.string.snake_highscore_preference_key), 0)));

        //region setGameFrameButtons
        mGameFrame.post(new Runnable() {
            @Override
            public void run() {
                ibLeft.getLayoutParams().width = mGameFrame.getWidth() / 2;
                ibRight.getLayoutParams().width = mGameFrame.getWidth() / 2;
            }
        });

        ibLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGame.snake.downPress();
            }
        });

        ibRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGame.snake.upPress();
            }
        });
        //endregion

        //region StartDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Ready?");
        final CharSequence[] items = {"yep!"};
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Create Game Object & Add Game to Frame

                dialog.dismiss();
                try {
                    doCountDown();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });


        builder.setCancelable(false);
        builder.show();
        //endregion

        //region ScoreUpdate
        mGame.setOnScoreUpdate(new ScoreUpdateCallback() {
            @Override
            public void callOnScoreUpdate(ScoreCounter counter) {
                tv_score.setText(String.format(
                        Locale.GERMAN,
                        getString(R.string.score_format_string),
                        counter.getScore()
                ));
            }
        });
        //endregion

    }

    /**
     * Display a countdown on the TextView located in the middle of the GameFrame
     * @throws InterruptedException
     *      thrown when the program is interrupted while it is currently sleeping
     */
    private void doCountDown() throws InterruptedException {
        mCountDown.post(new Runnable() {
            @Override
            public void run() {
                mCountDown.setVisibility(View.VISIBLE);
                mCountDown.setText("3");
            }
        });

        mCountDown.postDelayed(new Runnable() {
            @Override
            public void run() {
                mCountDown.setText("2");
            }
        }, 1000);

        mCountDown.postDelayed(new Runnable() {
            @Override
            public void run() {
                mCountDown.setText("1");
            }
        }, 2000);

        mCountDown.postDelayed(new Runnable() {
            @Override
            public void run() {
                //mCountDown.setVisibility(View.INVISIBLE);
                mCountDown.setText("");

                mGame.snake.stopped = false;
                mGame.invalidate();
            }
        }, 3000);

    }

    private void addButtonClickListener(FrameLayout up, FrameLayout down, FrameLayout left, FrameLayout right) {

        up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGame.snake.turnUp();
            }
        });
        down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGame.snake.turnDown();
            }
        });
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGame.snake.turnLeft();
            }
        });
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGame.snake.turnRight();
            }
        });

    }

    /**
     * creating a udp-sender using data from a SharedPreference object
     * @param ledWallPrefs
     *      object used to fetch information about ip and port from
     */
    public void setUpUdpSender(SharedPreferences ledWallPrefs) {
        String ip = ledWallPrefs.getString(
                getString(R.string.ip_preference_key),
                getString(R.string.default_ip));
        int port = (ledWallPrefs.getInt(
                getString(R.string.port_preference_key),
                getResources().getInteger(R.integer.default_port)));
        try {
            this.mSender = new UdpSender(InetAddress.getByName(ip), port);
        } catch (Exception ex) {
            this.mSender = null;
            Log.e(TAG, "Couldn't create Udp-Sender", ex);
        }
    }

    /**
     * Method that is executed when the snake has died. Checks if a new Highscore has been reached
     * and stores that highscore to SharedPreferences. Also posts that score to the ledwallscoreserver
     * using the current date and the name stored in SharedPreferences. Furthermore displays a
     */
    public void gameOver() {
        int oldHighscore = mLedWallPrefs.getInt(getString(R.string.snake_highscore_preference_key), 0);
        if(mGame.getScore() > oldHighscore) {
            ((TextView) findViewById(R.id.tv_highScore)).setText(String.format(
                    Locale.GERMAN,
                    "Highscore: %d",
                    mGame.getScore()));
        }

        String playerName = mLedWallPrefs.getString(getString(R.string.player_name_preference_key), getString(R.string.default_player_name));
        String defaultPlayerName = getString(R.string.default_player_name);
        final SnakeEndDialog dialog = new SnakeEndDialog(this, mGame.getScore(), playerName, defaultPlayerName, oldHighscore);
        dialog.setCancelable(false);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface d) {
                if(((SnakeEndDialog)d).getReturnCode() == SnakeEndDialog.Companion.getEXIT_CODE() ||
                        ((SnakeEndDialog)d).getReturnCode() == SnakeEndDialog.Companion.getDEFAULT_CODE()){
                    d.dismiss();
                    finish();
                    startActivity(new Intent(SnakeActivity.this, HomeActivity.class));
                }
                else if(((SnakeEndDialog)d).getReturnCode() == SnakeEndDialog.Companion.getRETRY_CODE()){
                    d.dismiss();
                    mGame.setup(mSender, true);
                    mGame.invalidate();
                    mGameFrame.requestLayout();

                    try {
                        doCountDown();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Log.i(TAG, "onDismiss: closed with resultCode " + ((SnakeEndDialog) d).getReturnCode());
                SnakeEndDialog.Companion.setOpen(false);
            }
        });
        if(!SnakeEndDialog.Companion.isOpen() ) {
            dialog.show();
            SnakeEndDialog.Companion.setOpen(true);
        }
        else{
            Log.i(TAG, "gameOver: prevented opening two dialogs at the same time");
        }
    }

    /**
     * Method executed when a hardware key has been pressed, if volume up or down, turn snake left
     * or right, respectively
     * @param keyCode
     *      the keycode of the key pressed
     * @param event
     *      containing information about the event, e.g.
     * @return
     *      always true, not used in my code
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode){
            case KeyEvent.KEYCODE_DPAD_UP:
                mGame.snake.turnUp();
                break;
            case KeyEvent.KEYCODE_DPAD_DOWN:
                mGame.snake.turnDown();
                break;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                mGame.snake.turnLeft();
                break;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                mGame.snake.turnRight();
                break;
        }
        return true;
    }

    //same as above
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();

        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (action == KeyEvent.ACTION_DOWN) {
                    mGame.snake.downPress();
                }
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (action == KeyEvent.ACTION_DOWN) {
                    mGame.snake.upPress();
                }
                return true;
            default:
                return super.dispatchKeyEvent(event);
        }
    }
}
