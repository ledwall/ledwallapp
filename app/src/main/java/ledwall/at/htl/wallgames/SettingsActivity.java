package ledwall.at.htl.wallgames;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import ledwall.at.htl.wallgames.Business.ElementSetter;
import ledwall.at.htl.wallgames.Entity.SnakeScore;
import ledwall.at.htl.wallgames.Networking.PostScoreAsyncTask;

/**
 * @author Tanzer, stoez
 * @version 5/23/2016
 */
public class SettingsActivity extends MenuActivity {
    private final String TAG = getClass().getSimpleName();

    private EditText mIp;
    private EditText mPort;
    private EditText mPlayerName;
    private CheckBox mListenForSmsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_settings, null, false);
        mDrawer.addView(contentView, 1);
        new ElementSetter().beautifyLayout(this);

        //region fetch view references
        mIp = (EditText) findViewById(R.id.ipAddress);
        mPort = (EditText) findViewById(R.id.port);
        FloatingActionButton fab_save = (FloatingActionButton) findViewById(R.id.fab_save);
        mListenForSmsButton = (CheckBox) findViewById(R.id.cb_listen);
        mPlayerName = (EditText) findViewById(R.id.playerName);
        Button btn_resetScores = (Button) findViewById(R.id.resetScores);
        //endregion

        //region fill fields with data from SharedPreferences
        final SharedPreferences prefs = getSharedPreferences(
                getString(R.string.shared_preferences_key),
                MODE_PRIVATE);

        mPlayerName.setText(prefs.getString(getString(R.string.player_name_preference_key),
                getString(R.string.default_player_name)));
        mIp.setText(
                prefs.getString(
                        getString(R.string.ip_preference_key),
                        getString(R.string.default_ip)));
        mPort.setText(
                String.format(
                        Locale.GERMAN,
                        "%d",
                        prefs.getInt(
                                getString(R.string.port_preference_key),
                                getResources().getInteger(R.integer.default_port))));

        final boolean[] wasChecked = {prefs.getBoolean(getString(R.string.listen_for_sms_key),
                getResources().getBoolean(R.bool.default_listen_for_sms_value))};
        mListenForSmsButton.setChecked(wasChecked[0]);
        //endregion

        fab_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            SharedPreferences.Editor prefEditor = prefs.edit();

            String ip = mIp.getText().toString();
            String portString = mPort.getText().toString();
                String playerName = mPlayerName.getText().toString();

            int port = -1;
            try {
                port = Integer.parseInt(portString);
            } catch (NumberFormatException ex) {
                Log.e(TAG, "Port not valid!", ex);
                ex.printStackTrace();
            }

            if(!ip.isEmpty() || port != -1 || mListenForSmsButton.isChecked() != wasChecked[0] || !playerName.isEmpty()) {
                if (!ip.isEmpty()) {
                    prefEditor.putString(getString(R.string.ip_preference_key), ip);
                    Log.d(TAG, "ip set to " + ip);
                }
                if (port != -1) {
                    prefEditor.putInt(getString(R.string.port_preference_key), port);
                    Log.d(TAG, "port set to " + port);
                }
                if(mListenForSmsButton.isChecked() != wasChecked[0]){
                    prefEditor.putBoolean(
                            getString(R.string.listen_for_sms_key)
                            , mListenForSmsButton.isChecked());
                    Log.d(TAG, "Listen for sms set to " +
                            Boolean.toString(mListenForSmsButton.isChecked()));
                    wasChecked[0] = mListenForSmsButton.isChecked();
                }
                if(!playerName.isEmpty()){
                    prefEditor.putString(getString(R.string.player_name_preference_key),
                            playerName);
                    Log.d(TAG, "player name changed to " + playerName);
                }

                prefEditor.apply();
                Toast.makeText(getApplicationContext(), "Settings have been saved", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(getApplicationContext(), "Settings couldn't be saved...", Toast.LENGTH_SHORT).show();
            }
            }
        });

        btn_resetScores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt(getString(R.string.snake_highscore_preference_key), 0);
                editor.apply();

                Toast.makeText(getApplicationContext(), "Your local highscores have been reset", Toast.LENGTH_SHORT).show();
            }
        });

        SharedPreferences highscorePrefs = getSharedPreferences(getString(R.string.highscores_shared_preference_key), Context.MODE_PRIVATE);
        if (highscorePrefs.contains(getString(R.string.highscore_object_preference_key))) {
            Log.d(TAG, "onCreate: trying to send the score again");
            try {
                SnakeScore score = new SnakeScore(new JSONObject(highscorePrefs.getString(getString(R.string.highscore_object_preference_key), "")));
                new PostScoreAsyncTask(getApplicationContext(), score).execute();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
