package ledwall.at.htl.wallgames.Business

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import java.util.Locale

import ledwall.at.htl.wallgames.Entity.SnakeScore
import ledwall.at.htl.wallgames.R
import java.text.SimpleDateFormat

/**
 * An RecyclerViewAdapter which helps access the list of SnakeScores fetched from the score-server
 * Basically just an improved version of the SnakeScoreArrayAdapter
 * @see SnakeScoreArrayAdapter
 * @author stoez
 * @version 1.0 31.05.2016
 */
class SnakeScoreAdapter(private val scoreList: List<SnakeScore>) : RecyclerView.Adapter<SnakeScoreAdapter.SnakeScoreViewHolder>() {
    private val sdf = SimpleDateFormat("d. M. yyyy", Locale.GERMAN)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SnakeScoreViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return SnakeScoreViewHolder(itemView)
    }

    override fun onBindViewHolder(viewHolder: SnakeScoreViewHolder, position: Int) {
        val s = scoreList[position]
        viewHolder.vScore.text = Integer.toString(s.score)

        if(position == 0){
            //your own highscore is always on the first position if a score server can be reached
            viewHolder.vName.text = "Your personal highscore!"
            viewHolder.vCreated.text = ""
        }
        else {
            viewHolder.vName.text = s.name
            viewHolder.vCreated.text = sdf.format(s.created)
        }
    }

    override fun getItemCount(): Int {
        return scoreList.size
    }

    class SnakeScoreViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var vScore: TextView
        var vName: TextView
        var vCreated: TextView

        init {
            vScore = v.findViewById(R.id.score) as TextView
            vName = v.findViewById(R.id.name) as TextView
            vCreated = v.findViewById(R.id.created) as TextView
        }
    }
}
