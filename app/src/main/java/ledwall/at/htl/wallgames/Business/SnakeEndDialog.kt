package ledwall.at.htl.wallgames.Business

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import ledwall.at.htl.wallgames.Entity.SnakeScore
import ledwall.at.htl.wallgames.Networking.PostScoreAsyncTask
import ledwall.at.htl.wallgames.R
import java.util.*

/**
 * @author stoez
 * *
 * @version 1.0 06.06.2016
 */
class SnakeEndDialog(private val a: Activity, private val score: Int, private var playerName:String, private val defaultPlayerName: String, private val oldHighScore : Int)
: Dialog(a){
    private var mLedWallPrefs: SharedPreferences? = null
    var returnCode: Int = DEFAULT_CODE
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mLedWallPrefs = a.getSharedPreferences(
                a.resources.getString(R.string.shared_preferences_key), Context.MODE_PRIVATE)

        val formatText: String

        if(defaultPlayerName.equals(playerName)){
            setContentView(R.layout.big_game_over_dialog)
        }else{
            setContentView(R.layout.small_game_over_dialog)
        }

        if(score > oldHighScore){
            setTitle(a.resources.getString(R.string.text_new_highscore))
            formatText = a.resources.getText(R.string.game_over_highscore_message).toString()
            mLedWallPrefs!!.edit().putInt(
                    a.resources.getString(R.string.snake_highscore_preference_key),
                    score).apply()

            if(!playerName.equals(defaultPlayerName)) {
                PostScoreAsyncTask(a.applicationContext, SnakeScore(score, playerName, java.sql.Date(Date().time))).execute()
            }
        }else{
            setTitle(a.resources.getString(R.string.game_over_title))
            formatText = a.resources.getText(R.string.game_over_message).toString()
        }
        (findViewById(R.id.tv_content) as TextView).text = String.format(formatText, score)

        if(playerName.equals(defaultPlayerName)){
            val btn_save = findViewById(R.id.btn_save) as Button
            val et_name = findViewById(R.id.et_name) as EditText

            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
            et_name.requestFocus()
            val imm =  a.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager;
            imm.showSoftInput(et_name, InputMethodManager.SHOW_IMPLICIT);


            btn_save.setOnClickListener {
                playerName = et_name.text.toString()
                mLedWallPrefs!!.edit()
                        .putString(
                                a.resources.getString(R.string.player_name_preference_key),
                                et_name.text.toString()
                        ).apply()
                Log.i(javaClass.simpleName, "set name in sharedPrefs to " + et_name.text.toString())

                if(score > oldHighScore){
                    PostScoreAsyncTask(a.applicationContext,
                            SnakeScore(score, playerName, java.sql.Date(Date().time))).execute()
                }

                findViewById(R.id.ll_newName).post{
                    findViewById(R.id.ll_newName).visibility = LinearLayout.GONE
                    findViewById(R.id.tv_score_saved).visibility = TextView.VISIBLE
                }
            }

        }

        val exitBtn = findViewById(R.id.btnExit) as Button
        exitBtn.setOnClickListener { v ->
            returnCode = SnakeEndDialog.EXIT_CODE
            dismiss()
        }
        val retryBtn = findViewById(R.id.btnRetry) as Button
        retryBtn.setOnClickListener { v ->
            returnCode = RETRY_CODE
            dismiss()
        }
    }

    companion object{
        val DEFAULT_CODE = -1;
        val EXIT_CODE = 0;
        var RETRY_CODE = 1;

        var isOpen : Boolean = false;
    }
}
