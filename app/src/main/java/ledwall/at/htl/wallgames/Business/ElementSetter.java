package ledwall.at.htl.wallgames.Business;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import ledwall.at.htl.wallgames.DisplaySnakeScoresActivity;
import ledwall.at.htl.wallgames.R;
import ledwall.at.htl.wallgames.SnakeActivity;

/**
 * A helping class that is used to adjust the layout of activities
 * @author Tanzer
 * @version 1.0 5/20/2016
 */
public class ElementSetter {
    private final String TAG = getClass().getSimpleName();

    public void beautifyLayout(Context context){
        int actionBarHeight = 0;
        final Activity activity = (Activity) context;

        TypedValue tv = new TypedValue();
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
        {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,context.getResources().getDisplayMetrics());
        }


        final RelativeLayout relativeLayout = (RelativeLayout)activity.findViewById(R.id.rLayout);
        final int finalActionBarHeight = actionBarHeight;
        relativeLayout.post(new Runnable() {
            @Override
            public void run() {
                final int fLayoutHeight = relativeLayout.getHeight();
                Log.i(TAG, "LayoutHeight: " + String.valueOf(fLayoutHeight));
                Log.i(TAG, "AppBarHeight: " + String.valueOf(finalActionBarHeight));
                setCorrectMargins(fLayoutHeight, finalActionBarHeight, activity);
            }
        });
    }

    private void setCorrectMargins(int fLayoutHeight, int finalActionBarHeight, Activity activity) {
        double topHeight = fLayoutHeight*0.45;
        double botHeight = fLayoutHeight*0.55;
        String name = activity.getClass().getSimpleName();
        LinearLayout topL = (LinearLayout) activity.findViewById(R.id.topLayout);
        LinearLayout botL = (LinearLayout) activity.findViewById(R.id.botLayout);

        if(name.contains(DisplaySnakeScoresActivity.class.getSimpleName())){
            topHeight = fLayoutHeight;
            botHeight = 0;
        }
        Log.i(TAG, "ActivityName: " + name);
        Log.i(TAG, "BotLayout Height: " + String.valueOf(botHeight));

        if(topL != null) {
            topL.setPadding(0, finalActionBarHeight, 0, 0);
            topL.getLayoutParams().height = (int) topHeight;
            topL.requestLayout();
        }

        if(botL != null) {
            if (!name.contains(SnakeActivity.class.getSimpleName())) {
                botL.setPadding(0, (finalActionBarHeight - 20), 0, 0);
            }

            botL.getLayoutParams().height = (int) botHeight;
            botL.requestLayout();


        }
    }
}
