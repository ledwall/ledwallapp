package ledwall.at.htl.wallgames;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import ledwall.at.htl.wallgames.Business.ElementSetter;
import ledwall.at.htl.wallgames.Entity.CustomDialog;
import ledwall.at.htl.wallgames.Networking.UdpSender;
import org.bitbucket.tuesd4y.ledwallapi.entity.Colour;
import org.bitbucket.tuesd4y.ledwallapi.entity.LedWallMode;
import org.bitbucket.tuesd4y.ledwallapi.networking.MessageSender;

import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * @author Tanzer, stoez
 * @version 5/27/2016
 */
public class SendTextActivity extends MenuActivity {
    private final String TAG = getClass().getSimpleName();

    private TextView mTextToShow;
    private TextView mTextMessage;
    public CustomDialog cdd;

    private final Colour[] mColour = {new Colour(255, 0, 0)};
    private MessageSender mSender;
    private String[] mSwearWords;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams")
        View contentView = inflater.inflate(R.layout.activity_send_text, null, false);
        mDrawer.addView(contentView, 1);

        new ElementSetter().beautifyLayout(this);

        //region fetching view references
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabSend);
        mTextToShow = (TextView) findViewById(R.id.slidingText);
        mTextMessage = (TextView) findViewById(R.id.message_text);
        Button changeColourButton = (Button) findViewById(R.id.btn_changeColour);
        //endregion

        cdd = new CustomDialog(SendTextActivity.this);
        mTextToShow.setTextColor(Color.RED);
        mTextMessage.setSingleLine();

        assert changeColourButton != null;
        changeColourButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cdd.show();
                cdd.findViewById(R.id.selectBtn).setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        int color = cdd.colorPicker.getColor();
                        Log.i(TAG, String.valueOf(color));
                        int r = (color >> 16) & 0xFF;
                        int g = (color >> 8) & 0xFF;
                        int b = (color) & 0xFF;
                        mColour[0] = new Colour(r, g, b);
                        cdd.dismiss();
                        mTextToShow.setTextColor(cdd.colorPicker.getColor());
                    }
                });
            }
        });

        final SharedPreferences prefs = getSharedPreferences(
                getString(R.string.shared_preferences_key),
                MODE_PRIVATE);
        mSwearWords = getResources().getStringArray(R.array.swear_words);

        //region FAB
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String res = "";
                String message = mTextMessage.getText().toString();

                if(!containsSwearWords(message)) {
                    try {
                        if(!message.isEmpty()) {
                            mSender = new UdpSender(
                                    InetAddress.getByName(prefs.getString(getString(R.string.ip_preference_key), getString(R.string.default_ip))),
                                    prefs.getInt(getString(R.string.port_preference_key), getResources().getInteger(R.integer.default_port))
                            );
                            mSender.sendModeChange(LedWallMode.MESSAGE);
                            mSender.sendMessage(message, mColour[0]);
                            mTextToShow.setText(message);
                            if (cdd.colorPicker != null) {
                                Log.i(TAG, "onClick: new color has been stored: " + cdd.colorPicker.getColor());
                            }
                            Log.d(TAG,
                                    "message sent to " + mSender.getIpAddress() +
                                            ":" + mSender.getPort() + " successfully");
                        }

                        res = "Message sent";

                    } catch (UnknownHostException e) {
                        res = "Ip-Adress not valid!";
                        Log.e(TAG, res, e);
                        e.printStackTrace();
                    } catch (IllegalArgumentException e) {
                        res = "Port not valid!";
                        Log.e(TAG, res, e);
                        e.printStackTrace();
                    } catch (ClassCastException | SocketException e) {
                        res = "Can't create MessageSender from IP-Address";
                        Log.e(TAG, res, e);
                        e.printStackTrace();
                    } finally {
                        if(message.isEmpty()){
                            res = "No message content specified...";
                            Log.e(TAG, "onClick: "+ res);
                        }
                        Snackbar.make(view, res, Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }
                else{
                    Log.i(TAG, getString(R.string.message_not_sent_text));
                    mTextToShow.setText("");
                    Snackbar.make(view, R.string.message_not_sent_text, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });
        //endregion
    }

    /**
     * checks if the given message contains any swear words
     * The list of swear-words should be filled before this method is executed
     * @param msg
     *      the message to check for swear words
     * @return
     *      true if there were any swear words
     *      false if the message is ok
     */
    private boolean containsSwearWords(String msg){
        for(String sw : mSwearWords){
            if(msg.toLowerCase().contains(sw))
                return true;
        }
        return false;
    }

}
