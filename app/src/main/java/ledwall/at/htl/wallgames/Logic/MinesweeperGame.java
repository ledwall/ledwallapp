package ledwall.at.htl.wallgames.Logic;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import ledwall.at.htl.wallgames.Entity.MinesweeperBlock;
import ledwall.at.htl.wallgames.Entity.SimpleBlock;
import ledwall.at.htl.wallgames.MinesweeperActivity;
import ledwall.at.htl.wallgames.R;
import org.bitbucket.tuesd4y.ledwallapi.entity.Block;
import org.bitbucket.tuesd4y.ledwallapi.networking.MessageSender;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Mosquera, minor changes by stoez
 * @version 5/27/2016
 * Colour { 0black, 1blue, 2green, 3red, 4white, 5rosy }
 */
public class MinesweeperGame {
    private MessageSender sender;
    private MinesweeperActivity minesweeperActivity;

    public MinesweeperGame(MessageSender sender, MinesweeperActivity minesweeperActivity){
        this.sender = sender;
        this.minesweeperActivity = minesweeperActivity;
    }

    /**
     * Method to start the game
     * Generate the field --> set the Mines
     * Show the field on the device
     * The Default total value of mine is 10 , because the LED-WALL is small.
     * @param minesweeperActivity
     */
    public void startNewGame(MinesweeperActivity minesweeperActivity)
    {
        // plant mines and do rest of the calculations
        createMineField
                (minesweeperActivity, minesweeperActivity.isGameOver, minesweeperActivity.numberOfColumnsInMineField, minesweeperActivity.numberOfRowsInMineField);
        // display all blocks in UI
        showMineField
                (minesweeperActivity, minesweeperActivity.blockDimension, minesweeperActivity.blockPadding, minesweeperActivity.blocks, (int)minesweeperActivity.height, minesweeperActivity.mineField, minesweeperActivity.numberOfColumnsInMineField, minesweeperActivity.numberOfRowsInMineField, minesweeperActivity.width);

        minesweeperActivity.minesToFind = minesweeperActivity.totalNumberOfMines;
        minesweeperActivity.isGameOver = false;
        minesweeperActivity.secondsPassed = 0;
    }
    /**
     * Method to show the field on the device
     * @param minesweeperActivity
     * @param blockDimension
     * @param blockPadding
     * @param blocks
     * @param height
     * @param mineField
     * @param numberOfColumnsInMineField
     * @param numberOfRowsInMineField
     * @param width
     */
    public void showMineField(MinesweeperActivity minesweeperActivity, int blockDimension, int blockPadding, MinesweeperBlock[][] blocks, int height, TableLayout mineField, int numberOfColumnsInMineField, int numberOfRowsInMineField, int width)
    {

        // 0th and last Row and Columns
        // are used for calculation purposes only
        for (int row = 1; row < numberOfRowsInMineField + 1; row++)
        {

            TableRow tableRow = new TableRow(minesweeperActivity);
            tableRow.setLayoutParams(new TableRow.LayoutParams((blockDimension + 2 * blockPadding) * numberOfColumnsInMineField, blockDimension + 2 * blockPadding));

            for (int column = 1; column < numberOfColumnsInMineField + 1; column++)
            {
                blocks[row][column].setLayoutParams(new TableRow.LayoutParams(
                        blockDimension + width * blockPadding,
                        blockDimension + height * blockPadding));
                blocks[row][column].setPadding(blockPadding, blockPadding, blockPadding, blockPadding);
                tableRow.addView(blocks[row][column]);
            }
            mineField.addView(tableRow, new TableLayout.LayoutParams(
                    (blockDimension + 2 * blockPadding) * numberOfColumnsInMineField, blockDimension + 2 * blockPadding));
        }

    }

    public void createMineField(final MinesweeperActivity minesweeperActivity, final boolean isGameOver, int numberOfColumnsInMineField, int numberOfRowsInMineField)
    {

        //Initialize the field
        minesweeperActivity.blocks = new MinesweeperBlock[numberOfRowsInMineField + 2][numberOfColumnsInMineField + 2];

        for (int row = 0; row < numberOfRowsInMineField + 2; row++)
        {
            for (int column = 0; column < numberOfColumnsInMineField + 2; column++)
            {
                minesweeperActivity.blocks[row][column] = new MinesweeperBlock(minesweeperActivity);
                //Set the default value . Default values are in SnakeBlock Class
                minesweeperActivity.blocks[row][column].setDefaults();

                //Set the listener on the current SnakeBlock and
                // set the random mines
                final int currentRow = row;
                final int currentColumn = column;

                // add Click Listener
                // this is treated as Left Mouse click
                minesweeperActivity.blocks[row][column].setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        //normal click LED BLUE
                        //mines RED LED
                        List<Block> blocks = new LinkedList<>();
                        // start timer on first click
                        if (!minesweeperActivity.isTimerStarted)
                        {
                            minesweeperActivity.startTimer();
                            minesweeperActivity.isTimerStarted = true;
                        }

                        // set mines on first click
                        if (!minesweeperActivity.areMinesSet)
                        {
                            minesweeperActivity.areMinesSet = true;
                            minesweeperActivity.setMines(currentRow, currentColumn);
                        }


                        if (!minesweeperActivity.blocks[currentRow][currentColumn].isFlagged())
                        {
                            // open nearby blocks till we get numbered blocks
                            minesweeperActivity.rippleUncover(currentRow, currentColumn, blocks);

                            // did we clicked a mine
                            if (minesweeperActivity.blocks[currentRow][currentColumn].hasMine())
                            {
                                // Oops, game over
                                finishGame(minesweeperActivity, minesweeperActivity.blocks, minesweeperActivity.btnSmile, minesweeperActivity.numberOfColumnsInMineField, minesweeperActivity.numberOfRowsInMineField, minesweeperActivity.secondsPassed, currentRow, currentColumn);
                                sender.sendBlock(SimpleBlock.of(currentRow, currentColumn, minesweeperActivity.blocks[currentRow][currentColumn]));
                            }

                            // check if we win the game
                            if (checkGameWin(minesweeperActivity.blocks, minesweeperActivity.numberOfColumnsInMineField, minesweeperActivity.numberOfRowsInMineField))
                            {
                                // mark game as win
                                winGame(minesweeperActivity, minesweeperActivity.blocks, minesweeperActivity.btnSmile, minesweeperActivity.numberOfColumnsInMineField, minesweeperActivity.numberOfRowsInMineField, minesweeperActivity.secondsPassed);
                            }
                        }
                    }
                });

                // add Long Click listener
                // this is treated as right mouse click listener
                minesweeperActivity.blocks[row][column].setOnLongClickListener(new View.OnLongClickListener() {
                    public boolean onLongClick(View view) {
                        List<Block> blocks = new LinkedList<>();
                        // open all surrounding blocks
                        if (!minesweeperActivity.blocks[currentRow][currentColumn].isCovered() && (minesweeperActivity.blocks[currentRow][currentColumn].countMinesSurrounding() > 0) && !isGameOver) {
                            int nearbyFlaggedBlocks = 0;
                            for (int previousRow = -1; previousRow < 2; previousRow++) {
                                for (int previousColumn = -1; previousColumn < 2; previousColumn++) {
                                    if (minesweeperActivity.blocks[currentRow + previousRow][currentColumn + previousColumn].isFlagged()) {
                                        nearbyFlaggedBlocks++;
                                    }
                                }
                            }

                            // if flagged block count is equal to nearby mine count
                            // then open nearby blocks
                            if (nearbyFlaggedBlocks == minesweeperActivity.blocks[currentRow][currentColumn].countMinesSurrounding()) {
                                for (int previousRow = -1; previousRow < 2; previousRow++) {
                                    for (int previousColumn = -1; previousColumn < 2; previousColumn++) {
                                        // don't open flagged blocks
                                        if (!minesweeperActivity.blocks[currentRow + previousRow][currentColumn + previousColumn].isFlagged()) {
                                            // open blocks till we get numbered block
                                            minesweeperActivity.rippleUncover(currentRow + previousRow, currentColumn + previousColumn, blocks);

                                            // did we clicked a mine
                                            if (minesweeperActivity.blocks[currentRow + previousRow][currentColumn + previousColumn].hasMine()) {
                                                // oops game over
                                                finishGame
                                                        (minesweeperActivity, minesweeperActivity.blocks, minesweeperActivity.btnSmile, minesweeperActivity.numberOfColumnsInMineField, minesweeperActivity.numberOfRowsInMineField, minesweeperActivity.secondsPassed, currentRow + previousRow, currentColumn + previousColumn);
                                            }

                                            // did we win the game
                                            if (checkGameWin
                                                    (minesweeperActivity.blocks, minesweeperActivity.numberOfColumnsInMineField, minesweeperActivity.numberOfRowsInMineField)) {
                                                // mark game as win
                                                winGame(minesweeperActivity, minesweeperActivity.blocks, minesweeperActivity.btnSmile, minesweeperActivity.numberOfColumnsInMineField, minesweeperActivity.numberOfRowsInMineField, minesweeperActivity.secondsPassed);
                                            }
                                        }
                                    }
                                }
                            }
                            sender.sendBlocks(blocks);
                            return true;
                        }

                        // if clicked block is enabled, clickable or flagged
                        if (minesweeperActivity.blocks[currentRow][currentColumn].isClickable() &&
                                (minesweeperActivity.blocks[currentRow][currentColumn].isEnabled() || minesweeperActivity.blocks[currentRow][currentColumn].isFlagged())) {

                            //MARKING FUNCTION: it will mark a  selected block will not open it.

                            // case 1. set blank block to flagged
                            if (!minesweeperActivity.blocks[currentRow][currentColumn].isFlagged() && !minesweeperActivity.blocks[currentRow][currentColumn].isQuestionMarked()) {
                                minesweeperActivity.blocks[currentRow][currentColumn].setBlockAsDisabled(false);
                                minesweeperActivity.blocks[currentRow][currentColumn].setFlagIcon(true);
                                minesweeperActivity.blocks[currentRow][currentColumn].setFlagged(true);
                                minesweeperActivity.minesToFind--; //reduce mine count
                                minesweeperActivity.updateMineCountDisplay();
                            }
                            // case 2. set flagged to question mark
                            else if (!minesweeperActivity.blocks[currentRow][currentColumn].isQuestionMarked()) {
                                minesweeperActivity.blocks[currentRow][currentColumn].setBlockAsDisabled(true);
                                minesweeperActivity.blocks[currentRow][currentColumn].setQuestionMarkIcon(true);
                                minesweeperActivity.blocks[currentRow][currentColumn].setFlagged(false);
                                minesweeperActivity.blocks[currentRow][currentColumn].setQuestionMarked(true);
                                minesweeperActivity.minesToFind++; // increase mine count
                                minesweeperActivity.updateMineCountDisplay();
                            }
                            // case 3. change to blank square
                            else {
                                minesweeperActivity.blocks[currentRow][currentColumn].setBlockAsDisabled(true);
                                minesweeperActivity.blocks[currentRow][currentColumn].clearAllIcons();
                                minesweeperActivity.blocks[currentRow][currentColumn].setQuestionMarked(false);
                                // if it is flagged then increment mine count
                                if (minesweeperActivity.blocks[currentRow][currentColumn].isFlagged()) {
                                    minesweeperActivity.minesToFind++; // increase mine count
                                    minesweeperActivity.updateMineCountDisplay();
                                }
                                // remove flagged status
                                minesweeperActivity.blocks[currentRow][currentColumn].setFlagged(false);
                            }
                            sender.sendBlock(SimpleBlock.of(currentRow, currentColumn, minesweeperActivity.blocks[currentRow][currentColumn]));
                            minesweeperActivity.updateMineCountDisplay(); // update mine display
                        }
                        return true;
                    }
                });
            }
        }
    }



    /**
     * Method to end game and reset the values.
     * @param btnSmile
     * @param mineField
     * @param txtMineCount
     * @param txtTimer
     */
    public void endExistingGame(ImageButton btnSmile, TableLayout mineField, TextView txtMineCount, TextView txtTimer)
    {
        minesweeperActivity.stopTimer(); // stop if timer is running
        txtTimer.setText("000"); // revert all text
        txtMineCount.setText("000"); // revert mines count
        btnSmile.setBackgroundResource(R.drawable.smile);

        // remove all rows from mineField TableLayout
        mineField.removeAllViews();

        // set all variables to support end of game
        minesweeperActivity.isTimerStarted=false;
        minesweeperActivity.areMinesSet=false;
        minesweeperActivity.isGameOver=false;
        minesweeperActivity.minesToFind=0;

    }

    public boolean checkGameWin(MinesweeperBlock[][] blocks, int numberOfColumnsInMineField, int numberOfRowsInMineField)
    {
        for (int row = 1; row < numberOfRowsInMineField + 1; row++)
        {
            for (int column = 1; column < numberOfColumnsInMineField + 1; column++)
            {
                if (!blocks[row][column].hasMine() && blocks[row][column].isCovered())
                {
                    return false;
                }
            }
        }
        return true;
    }

    public void finishGame(MinesweeperActivity minesweeperActivity, MinesweeperBlock[][] blocks, ImageButton btnSmile, int numberOfColumnsInMineField, int numberOfRowsInMineField, int secondsPassed, int currentRow, int currentColumn)
    {
        minesweeperActivity.isGameOver = true; // mark game as over
        minesweeperActivity.stopTimer(); // stop timer
        minesweeperActivity.isTimerStarted = false;
        btnSmile.setBackgroundResource(R.drawable.sad);

        // show all mines
        // disable all blocks
        //so the user can't interact after game over
        for (int row = 1; row < numberOfRowsInMineField + 1; row++)
        {
            for (int column = 1; column < numberOfColumnsInMineField + 1; column++)
            {
                // disable block
                blocks[row][column].setBlockAsDisabled(false);
                //blocks[row][column].setClickable(false).... for some reason not working ,
                //so I'll set the listener to null
                blocks[row][column].setOnClickListener(null);
                blocks[row][column].setOnLongClickListener(null);

                // block has mine and is not flagged
                if (blocks[row][column].hasMine() && !blocks[row][column].isFlagged())
                {
                    // set mine icon
                    blocks[row][column].setMineIcon(false);
                    sender.sendBlock(SimpleBlock.of(row, column, blocks[row][column]));
                }

                // block is flagged and doesn't not have mine
                if (!blocks[row][column].hasMine() && blocks[row][column].isFlagged())
                {
                    // set flag icon
                    blocks[row][column].setFlagIcon(false);
                }

                // block is flagged
                if (blocks[row][column].isFlagged())
                {
                    // disable the block
                    blocks[row][column].setClickable(false);
                }
            }
        }

        // trigger mine
        blocks[currentRow][currentColumn].triggerMine();

        // show message
        minesweeperActivity.showDialog("You tried for " + Integer.toString(secondsPassed) + " seconds!", 1000, false, false);
    }

    public void winGame(MinesweeperActivity minesweeperActivity, MinesweeperBlock[][] blocks, ImageButton btnSmile, int numberOfColumnsInMineField, int numberOfRowsInMineField, int secondsPassed)
    {
        minesweeperActivity.stopTimer();
        minesweeperActivity.isTimerStarted = false;
        minesweeperActivity.isGameOver = true;
        minesweeperActivity.minesToFind = 0; //set mine count to 0

        //set icon to skull dude
        btnSmile.setBackgroundResource(R.drawable.cool);

        minesweeperActivity.updateMineCountDisplay(); // update mine count

        // disable all buttons
        // set flagged all un-flagged blocks
        for (int row = 1; row < numberOfRowsInMineField + 1; row++)
        {
            for (int column = 1; column < numberOfColumnsInMineField + 1; column++)
            {
                blocks[row][column].setClickable(false);
                if (blocks[row][column].hasMine())
                {
                    blocks[row][column].setBlockAsDisabled(false);
                    //blocks[row][column].setClickable(false).... for some reason not working ,
                    //so I'll set the listener to null
                    blocks[row][column].setOnClickListener(null);
                    blocks[row][column].setOnLongClickListener(null);
                    blocks[row][column].setFlagIcon(true);
                }
            }
        }

        // show message
        minesweeperActivity.showDialog("You won in " + Integer.toString(secondsPassed) + " seconds!", 1000, false, true);
    }





}
